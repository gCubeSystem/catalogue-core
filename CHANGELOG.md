This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Catalogue Core Library

## [v1.0.2]

- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]


## [v1.0.1]

- Groups is created with title #27716
- CKANPackageTrash constructor with configuration as paramter made public
- The removal of a file persited is resolved using the public link #27952


## [v1.0.0]

- First Version

