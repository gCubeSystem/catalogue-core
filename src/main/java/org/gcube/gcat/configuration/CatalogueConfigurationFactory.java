package org.gcube.gcat.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.configuration.isproxies.ISConfigurationProxy;
import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;
import org.gcube.gcat.configuration.isproxies.impl.FacetBasedISConfigurationProxyFactory;
import org.gcube.gcat.configuration.isproxies.impl.GCoreISConfigurationProxyFactory;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;
import org.gcube.gcat.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CatalogueConfigurationFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(CatalogueConfigurationFactory.class);
	
	private static final Map<String, ServiceCatalogueConfiguration> catalogueConfigurations;
	
	private static List<ISConfigurationProxyFactory<?>> factories;
	
	static {
		catalogueConfigurations = new HashMap<>();
		factories = new ArrayList<>();
	}
	
	private static List<ISConfigurationProxyFactory<?>> getFactories(){
		if(factories.size()==0) {
			factories.add(new FacetBasedISConfigurationProxyFactory());
			factories.add(new GCoreISConfigurationProxyFactory());
		}
		return factories;
	}
	
	public static void addISConfigurationProxyFactory(ISConfigurationProxyFactory<?> icpf) {
		factories.add(icpf);
	}
	
	private static ServiceCatalogueConfiguration load(String context) {
		ServiceCatalogueConfiguration serviceCatalogueConfiguration = null;
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
		
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				try {
					ISConfigurationProxy<?> icp = icpf.getInstance(context);
					
					serviceCatalogueConfiguration = icp.getCatalogueConfiguration();
					logger.trace("The configuration has been read using {}.", icp.getClass().getSimpleName());
				}catch(Exception e){
					logger.warn("{} cannot be used to read {}. Reason is {}", icpf.getClass().getSimpleName(), ServiceCatalogueConfiguration.class.getSimpleName(), e.getMessage());
				}
			}
			
		} catch(Exception e) {
			logger.error("Unable to start session. Reason is " + e.getMessage());
		} finally {
			secretManager.endSession();
		}
		
		if(serviceCatalogueConfiguration==null) {
			throw new RuntimeException("Unable to load " + ServiceCatalogueConfiguration.class.getSimpleName() + " by using configured " + ISConfigurationProxyFactory.class.getSimpleName() + " i.e. " + getFactories());
		}
		return serviceCatalogueConfiguration;
	}
	
	private static void purgeFromIS(String context) {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				ISConfigurationProxy<?> icp = icpf.getInstance(context);
				icp.delete();
			}
		} catch(Exception e) {
			logger.error("Unable to start session. Reason is " + e.getMessage());
		} finally {
			secretManager.endSession();
		}
	}
	
	private static void createOrUpdateOnIS(String context, ServiceCatalogueConfiguration catalogueConfiguration) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
			
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				ISConfigurationProxy<?> icp = icpf.getInstance(context);
				icp.setCatalogueConfiguration(catalogueConfiguration);
				icp.createOrUpdateOnIS();
			}
			
		} finally {
			secretManager.endSession();
		}
	}
	
	public synchronized static ServiceCatalogueConfiguration getInstance() {
		String context = SecretManagerProvider.instance.get().getContext();
		ServiceCatalogueConfiguration catalogueConfiguration = catalogueConfigurations.get(context);
		if(catalogueConfiguration == null) {
			catalogueConfiguration = load(context);
			catalogueConfigurations.put(context, catalogueConfiguration);
		}
		return catalogueConfiguration;
	}
	
	public synchronized static void renew() {
		String context = SecretManagerProvider.instance.get().getContext();
		catalogueConfigurations.remove(context);
		ServiceCatalogueConfiguration catalogueConfiguration = load(context);
		catalogueConfigurations.put(context, catalogueConfiguration);
	}
	
	public synchronized static void purge() {
		// Remove the resource from IS
		String context = SecretManagerProvider.instance.get().getContext();
		catalogueConfigurations.remove(context);
		purgeFromIS(context);
	}
	
	public synchronized static ServiceCatalogueConfiguration createOrUpdate(ServiceCatalogueConfiguration catalogueConfiguration) throws Exception {
		String context = SecretManagerProvider.instance.get().getContext();
		catalogueConfigurations.remove(context);
		
		createOrUpdateOnIS(context, catalogueConfiguration);
		catalogueConfigurations.put(context, catalogueConfiguration);
		
		// The supported organizations could be changed we need to empty the user cache for the context
		// to avoid to miss to add an user in an organization which has been added.
		CKANUserCache.emptyUserCache();
		
		return catalogueConfiguration;
	}
	
}
