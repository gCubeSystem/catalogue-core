package org.gcube.gcat.configuration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Version implements Comparable<Version> {

	/**
	 * Regex validating the version
	 */
	public final static String VERSION_REGEX = "^[1-9][0-9]{0,}\\.(0|([1-9][0-9]{0,}))\\.(0|([1-9][0-9]{0,}))";
	
	private final static Pattern VERSION_PATTERN; 
	
	static {
		VERSION_PATTERN  = Pattern.compile(VERSION_REGEX);
	}
	
	protected int major;
	protected int minor;
	protected int revision;
	
	protected Version(){}
	
	public Version(String version) {
		setVersion(version);
	}
	
	public Version(int major, int minor, int revision) {
		this.major = major;
		this.minor = minor;
		this.revision = revision;
	}
	
	public void setVersion(String version) {
		Matcher matcher = VERSION_PATTERN.matcher(version); 
		if(!matcher.find()) {
			throw new RuntimeException("The provided version (i.e. " + version + ") MUST comply with the regex " + VERSION_REGEX);
		}
		
		String matched = matcher.group(0);
		String[] parts = matched.split("\\.");
		this.major = Integer.valueOf(parts[0]);
		this.minor = Integer.valueOf(parts[1]);
		this.revision = Integer.valueOf(parts[2]);
	}
	
	public int getMajor() {
		return major;
	}

	protected void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	protected void setMinor(int minor) {
		this.minor = minor;
	}

	public int getRevision() {
		return revision;
	}

	protected void setRevision(int revision) {
		this.revision = revision;
	}

	@Override
	public String toString() {
		return major + "." + minor + "." + revision;
	}
	@Override
	public int compareTo(Version other) {
		if(other == null) {
            return 1;
		}
		
		int compare = Integer.compare(major, other.major);
		if(compare!=0) {
			return compare;
		}
		
		compare = Integer.compare(minor, other.minor);
		if(compare!=0) {
			return compare;
		}
		
		compare = Integer.compare(revision, other.revision);
		return compare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + major;
		result = prime * result + minor;
		result = prime * result + revision;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Version other = (Version) obj;
		if (major != other.major)
			return false;
		if (minor != other.minor)
			return false;
		if (revision != other.revision)
			return false;
		return true;
	}
	
}