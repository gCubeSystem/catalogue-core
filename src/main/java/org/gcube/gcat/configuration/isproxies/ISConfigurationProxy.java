package org.gcube.gcat.configuration.isproxies;

import javax.ws.rs.WebApplicationException;

import org.gcube.gcat.configuration.Version;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.context.application.ApplicationContext;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ISConfigurationProxy<ISResource extends Object> {

	protected final String context;
	protected ServiceCatalogueConfiguration catalogueConfiguration;
	
	public ISConfigurationProxy(String context) {
		this.context = context;
	}

	public ISConfigurationProxy(String context, ServiceCatalogueConfiguration catalogueConfiguration) {
		this(context);
		this.catalogueConfiguration = catalogueConfiguration;
	}
	
	public ServiceCatalogueConfiguration getCatalogueConfiguration() throws WebApplicationException {
		if (catalogueConfiguration == null) {
			catalogueConfiguration = readFromIS();
		}
		return catalogueConfiguration;
	}
	
	public void setCatalogueConfiguration(ServiceCatalogueConfiguration catalogueConfiguration) {
		this.catalogueConfiguration = catalogueConfiguration;
	}

	public ServiceCatalogueConfiguration createOrUpdateOnIS() throws Exception {
		ISResource isResource = getISResource();
		if(isResource!=null) {
			// It's an update
			catalogueConfiguration = updateOnIS();
		}else {
			// It's a create
			catalogueConfiguration = createOnIS();
		}
		return catalogueConfiguration;
	}
	
	protected Version getGcatVersion() {
		try {
			ApplicationContext applicationContext = ContextProvider.get();
			ApplicationConfiguration applicationConfiguration = applicationContext.configuration();
			Version version = new Version(applicationConfiguration.version());
			return version;
		}catch (Exception e) {
			return new Version("2.4.2");
		}
	}
	
	protected abstract ServiceCatalogueConfiguration createOnIS() throws Exception;
	
	protected abstract ISResource getISResource();

	protected abstract ServiceCatalogueConfiguration readFromIS();

	protected abstract ServiceCatalogueConfiguration updateOnIS() throws Exception;
	
	public abstract void delete();
	
}
