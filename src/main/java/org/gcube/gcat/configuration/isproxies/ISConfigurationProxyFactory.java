package org.gcube.gcat.configuration.isproxies;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ISConfigurationProxyFactory<ISCP extends ISConfigurationProxy<?>> {

	protected final Map<String, ISCP> isConfigurationProxies;
	
	public ISConfigurationProxyFactory() {
		this.isConfigurationProxies = new HashMap<>();
	}
	
	protected abstract ISCP newInstance(String context);
	
	public synchronized ISCP getInstance(String context) {
		ISCP isConfigurationProxy = isConfigurationProxies.get(context);
		if(isConfigurationProxy == null) {
			isConfigurationProxy = newInstance(context);
			isConfigurationProxies.put(context, isConfigurationProxy);
		}
		return isConfigurationProxy;
	}
	
	public ISCP getInstance() {
		String context = SecretManagerProvider.instance.get().getContext();
		return getInstance(context);
	}
	
}

