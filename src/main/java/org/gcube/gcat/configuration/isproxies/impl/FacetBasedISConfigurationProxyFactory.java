package org.gcube.gcat.configuration.isproxies.impl;

import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetBasedISConfigurationProxyFactory extends ISConfigurationProxyFactory<FacetBasedISConfigurationProxy> {

	public FacetBasedISConfigurationProxyFactory() {
		super();
	}

	@Override
	protected FacetBasedISConfigurationProxy newInstance(String context) {
		return new FacetBasedISConfigurationProxy(context);
	}

	
}
