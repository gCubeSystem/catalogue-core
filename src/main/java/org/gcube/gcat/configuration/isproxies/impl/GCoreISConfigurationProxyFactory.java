package org.gcube.gcat.configuration.isproxies.impl;

import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCoreISConfigurationProxyFactory extends ISConfigurationProxyFactory<GCoreISConfigurationProxy> {
	
	public GCoreISConfigurationProxyFactory() {
		super();
	}
	
	@Override
	protected GCoreISConfigurationProxy newInstance(String context) {
		GCoreISConfigurationProxy isConfigurationProxy = new GCoreISConfigurationProxy(context);
		return isConfigurationProxy;
	}

	
	
	
}
