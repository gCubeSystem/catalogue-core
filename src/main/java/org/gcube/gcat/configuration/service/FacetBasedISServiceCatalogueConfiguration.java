package org.gcube.gcat.configuration.service;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.gcat.configuration.isproxies.impl.FacetBasedISConfigurationProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetBasedISServiceCatalogueConfiguration extends ServiceCatalogueConfiguration {

	private static Logger logger = LoggerFactory.getLogger(FacetBasedISServiceCatalogueConfiguration.class);
	
	protected final FacetBasedISConfigurationProxy facetBasedISConfigurationProxy;
	
	public FacetBasedISServiceCatalogueConfiguration(String context, FacetBasedISConfigurationProxy facetBasedISConfigurationProxy) {
		super(context);
		this.facetBasedISConfigurationProxy = facetBasedISConfigurationProxy;
	}
	
	@JsonProperty(value = CKAN_URL_KEY)
	public String getCkanURL() {
		if(ckanURL==null) {
			try {
				facetBasedISConfigurationProxy.setCkanServiceInfo(this);
			} catch (Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
		return ckanURL;
	}
	
	@JsonIgnore
	public String getSysAdminToken() {
		if(sysAdminToken==null) {
			try {
				facetBasedISConfigurationProxy.setCkanServiceInfo(this);
			} catch (Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
		return sysAdminToken;
	}
	
	@JsonGetter(value=SYS_ADMIN_TOKEN_KEY)
	public String getEncryptedSysAdminToken() {
		if(encryptedSysAdminToken==null) {
			try {
				facetBasedISConfigurationProxy.setCkanServiceInfo(this);
			} catch (Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
		return encryptedSysAdminToken;
	}
	
	@JsonGetter(value = CKAN_DB_KEY)
	public ServiceCKANDB getCkanDB() {
		if(ckanDB==null) {
			try {
				facetBasedISConfigurationProxy.setCkanDBInfo(this);
			} catch (Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
		return (ServiceCKANDB) ckanDB;
	}
	
	
	@JsonProperty(value = SOLR_URL_KEY)
	public String getSolrURL() {
		if(solrURL==null) {
			try {
				facetBasedISConfigurationProxy.setSolrServiceInfo(this);
			} catch (Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
		return solrURL;
	}
}
