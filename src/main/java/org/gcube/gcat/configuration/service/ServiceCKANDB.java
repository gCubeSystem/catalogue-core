package org.gcube.gcat.configuration.service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.gcat.api.configuration.CKANDB;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class ServiceCKANDB extends CKANDB {

	public static final String USERNAME_KEY = "username";
	public static final String PASSWORD_KEY = "password";
	
	protected String encryptedPassword;
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	
	@JsonIgnore
	public String getPlainPassword() {
		return password;
	}
	
	@JsonGetter(value=PASSWORD_KEY)
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	
	public void setEncryptedPassword(String encryptedPassword) throws Exception {
		this.encryptedPassword = encryptedPassword;
		this.password = StringEncrypter.getEncrypter().decrypt(encryptedPassword);
	}
	
	public void setPlainPassword(String plainPassword) throws Exception {
		this.password = plainPassword;
		this.encryptedPassword = StringEncrypter.getEncrypter().encrypt(plainPassword);
	}
	
	@Override
	@JsonSetter(value = PASSWORD_KEY)
	public void setPassword(String password) {
		try {
			try {
				this.password = StringEncrypter.getEncrypter().decrypt(password);
				this.encryptedPassword = password;
			}catch (IllegalBlockSizeException | BadPaddingException e) {
				this.password = password;
				this.encryptedPassword = StringEncrypter.getEncrypter().encrypt(password);
			}
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
