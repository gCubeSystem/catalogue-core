package org.gcube.gcat.configuration.service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.gcat.api.configuration.CKANDB;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKANUser;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class ServiceCatalogueConfiguration extends CatalogueConfiguration {

	protected ObjectMapper mapper;
	
	public ServiceCatalogueConfiguration() {
		super();
		mapper = new ObjectMapper();
	}
	
	public ServiceCatalogueConfiguration(String context) {
		super(context);
		mapper = new ObjectMapper();
	}

	@JsonIgnore
	protected String encryptedSysAdminToken;
	
	@JsonIgnore
	public String getSysAdminToken() {
		return sysAdminToken;
	}
	
	@JsonIgnore
	public String getPlainSysAdminToken() {
		return getSysAdminToken();
	}
	
	@JsonGetter(value=SYS_ADMIN_TOKEN_KEY)
	public String getEncryptedSysAdminToken() {
		return encryptedSysAdminToken;
	}
	
	public void setEncryptedSysAdminToken(String encryptedSysAdminToken) throws Exception {
		this.encryptedSysAdminToken = encryptedSysAdminToken;
		this.sysAdminToken = StringEncrypter.getEncrypter().decrypt(encryptedSysAdminToken);
	}
	
	public void setPlainSysAdminToken(String plainSysAdminToken) throws Exception {
		this.sysAdminToken = plainSysAdminToken;
		this.encryptedSysAdminToken = StringEncrypter.getEncrypter().encrypt(plainSysAdminToken);
	}
	
	@Override
	@JsonSetter(value = SYS_ADMIN_TOKEN_KEY)
	public void setSysAdminToken(String sysAdminToken) {
		try {
			try {
				this.sysAdminToken = StringEncrypter.getEncrypter().decrypt(sysAdminToken);
				this.encryptedSysAdminToken = sysAdminToken;
			}catch (IllegalBlockSizeException | BadPaddingException e) {
				this.sysAdminToken = sysAdminToken;
				this.encryptedSysAdminToken = StringEncrypter.getEncrypter().encrypt(sysAdminToken);
			}
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	@JsonGetter(value = CKAN_DB_KEY)
	public ServiceCKANDB getCkanDB() {
		return (ServiceCKANDB) ckanDB;
	}
	
	@Override
	public void setCkanDB(CKANDB ckanDB) {
		this.ckanDB = new ServiceCKANDB();
		this.ckanDB.setUrl(ckanDB.getUrl());
		this.ckanDB.setUsername(ckanDB.getUsername());
		this.ckanDB.setPassword(ckanDB.getPassword());
	}
	
	@JsonSetter(value=CKAN_DB_KEY)
	public void setCkanDB(ServiceCKANDB ckanDB) {
		this.ckanDB = ckanDB;
	}
	
	public ObjectNode toObjetcNode() throws JsonProcessingException {
		return toObjetcNode(false);
	}
	
	public ObjectNode toObjetcNode(boolean decryptedValues) throws JsonProcessingException {
		ObjectNode configuration = mapper.valueToTree(this);
		CKANUser ckanUser = CKANUserCache.getCurrrentCKANUser();
		if(ckanUser.getRole().ordinal() < Role.MANAGER.ordinal()) {
			configuration.remove(ServiceCatalogueConfiguration.SYS_ADMIN_TOKEN_KEY);
			configuration.remove(ServiceCatalogueConfiguration.CKAN_DB_KEY);
		}else {
			if(decryptedValues) {
				configuration.put(ServiceCatalogueConfiguration.SYS_ADMIN_TOKEN_KEY, getPlainSysAdminToken());
				ObjectNode node = (ObjectNode) configuration.get(ServiceCatalogueConfiguration.CKAN_DB_KEY);
				node.put(ServiceCKANDB.PASSWORD_KEY, ((ServiceCKANDB) ckanDB).getPlainPassword());
			}
		}
		return configuration;
	}
	
	public String toJsonString() throws Exception {
		return toJsonString(false);
	}
	
	public String toJsonString(boolean decryptedValues) throws Exception {
		ObjectNode objectNode = toObjetcNode(decryptedValues);
		return mapper.writeValueAsString(objectNode);
	}
	
	public static <C extends CatalogueConfiguration> C getServiceCatalogueConfiguration(String json, Class<C> clz) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		C c = mapper.readValue(json, clz);
		return c;
	}
	
	public static <C extends CatalogueConfiguration> C getServiceCatalogueConfiguration(ObjectNode objectNode, Class<C> clz) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		C c = mapper.treeToValue(objectNode, clz);
		return c;
	}
	
	
	
}
