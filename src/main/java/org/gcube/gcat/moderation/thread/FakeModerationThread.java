package org.gcube.gcat.moderation.thread;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FakeModerationThread extends ModerationThread {

	private static final Logger logger = LoggerFactory.getLogger(FakeModerationThread.class);
	
	@Override
	protected void postMessage(String message) throws Exception {
		logger.info("gCat is sending a message to the {} for item '{}' (id={}). ItemStatus={}, Message=\"{}\"",
				ModerationThread.class.getSimpleName(), itemName, itemID, cmItemStatus, message);
	}

	@Override
	public void postUserMessage(CMItemStatus cmItemStatus, String userMessage) throws Exception {
		logger.info("{} is sending a message to the {} for item '{}' (id={}). ItemStatus={}, Message=\"{}\"",
				SecretManagerProvider.instance.get().getUser().getUsername(), 
				ModerationThread.class.getSimpleName(), itemName, itemID, cmItemStatus, userMessage);
	}

	@Override
	protected void createModerationThread() throws Exception {
		logger.info("Creating {} for item '{}' (id={})", ModerationThread.class.getSimpleName(), itemName, itemID);
	}


}
