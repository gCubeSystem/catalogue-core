package org.gcube.gcat.moderation.thread;

import java.util.HashMap;
import java.util.Map;

//import java.util.HashMap;
//import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
//import org.gcube.common.authorization.utils.manager.SecretManager;
//import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
//import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.moderation.thread.social.notifications.SocialNotificationModerationThread;
import org.gcube.gcat.persistence.ckan.CKANUser;
//import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
//import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryString.MODERATION_OP;
//import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryStringBuilder;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryString.MODERATION_OP;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryStringBuilder;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ModerationThread {

	protected String itemID;
	protected String itemName;
	protected String itemTitle;
	protected String itemURL;
	protected String itemAuthorCkanUsername;
	
	protected String moderationURL;

	protected boolean create;
	protected CMItemStatus cmItemStatus;
	protected boolean itemAuthor;

	protected CKANUser ckanUser;
	protected ObjectMapper objectMapper;

	public static ModerationThread getDefaultInstance() {
		// return new FakeModerationThread();
		// return new SocialMessageModerationThread();
		return new SocialNotificationModerationThread();
	}

	public ModerationThread() {
		this.objectMapper = new ObjectMapper();
		this.itemAuthor = false;
		this.create = false;
		this.cmItemStatus = CMItemStatus.PENDING;
	}

	public void setItemCoordinates(String itemID, String itemName, String itemTitle, String itemURL) {
		this.itemID = itemID;
		this.itemName = itemName;
		this.itemTitle = itemTitle;
		this.itemURL = itemURL;
	}

	public void setItemAuthor(boolean itemAuthor) {
		this.itemAuthor = itemAuthor;
	}

	public String getItemAuthorCkanUsername() {
		return itemAuthorCkanUsername;
	}

	public void setItemAuthorCkanUsername(String itemAuthorCkanUsername) {
		this.itemAuthorCkanUsername = itemAuthorCkanUsername;
	}

	public void setCKANUser(CKANUser ckanUser) {
		this.ckanUser = ckanUser;
	}

	public String getModerationURL() {
		if(moderationURL==null) {
			try {
				SecretManager secretManager = SecretManagerProvider.instance.get();
				String context = secretManager.getContext();
				UriResolverManager resolver = new UriResolverManager("CTLG");
				Map<String, String> params = new HashMap<String, String>();
				params.put("gcube_scope", context); //e.g. /gcube/devsec/devVRE
				params.put("entity_context", "organization");
				params.put("entity_name", CatalogueConfiguration.getOrganizationName(context)); //e.g. devvre
			 
				CatalogueResolverQueryStringBuilder builder = new CatalogueResolverQueryStringBuilder(itemName); //item name under moderation
				builder.itemStatus(cmItemStatus.name()). //e.g. pending, approved, rejected
				moderation(MODERATION_OP.show);
			 
				String queryString = builder.buildQueryParametersToQueryString();
				params.put(CatalogueResolverQueryStringBuilder.QUERY_STRING_PARAMETER, queryString);
			 
				moderationURL = resolver.getLink(params, true);
			}catch (Exception e) {
				return itemURL;
			}
		}
		return moderationURL;
	}
	
	/**
	 * The message is sent as gCat
	 * @param message
	 * @throws Exception
	 */
	protected abstract void postMessage(String message) throws Exception;

	/**
	 * The message is sent as User
	 * 
	 * @param cmItemStatus
	 * @param userMessage
	 * @throws Exception
	 */
	public abstract void postUserMessage(CMItemStatus cmItemStatus, String userMessage) throws Exception;

	protected abstract void createModerationThread() throws Exception;

	public void postItemCreated() throws Exception {
		createModerationThread();
		this.create = true;
		this.cmItemStatus = CMItemStatus.PENDING;
		String fullName = ckanUser.getNameSurname();
		String message = String.format(
				"@**%s** created the item with name '%s' (id='%s'). The item is now in **%s** state and must be moderated.",
				fullName, itemName, itemID, cmItemStatus.getFancyValue());
		postMessage(message);
	}

	public void postItemUpdated() throws Exception {
		this.cmItemStatus = CMItemStatus.PENDING;
		String fullName = ckanUser.getNameSurname();
		String message = String.format(
				"@**%s** updated the item with name '%s' (id='%s'). The item is now in **%s** state and must be moderated.",
				fullName, itemName, itemID, cmItemStatus.getFancyValue());
		postMessage(message);
	}

	public void postItemRejected(String userMessage) throws Exception {
		this.cmItemStatus = CMItemStatus.REJECTED;
		String fullName = ckanUser.getNameSurname();
		String message = String.format(
				"@**%s** **%s** the item with name '%s' (id='%s'). The author can delete the item or update it to try to meet moderators requests if any.",
				fullName, cmItemStatus.getFancyValue(), itemName, itemID);
		postMessage(message);
		postUserMessage(cmItemStatus, userMessage);
	}

	public void postItemApproved(String userMessage) throws Exception {
		this.cmItemStatus = CMItemStatus.APPROVED;
		String fullName = ckanUser.getNameSurname();
		String message = String.format(
				"@**%s** **%s** the item with name '%s' (id='%s'). The item is now available in the catalogue. The item is available at %s",
				fullName, cmItemStatus.getFancyValue(), itemName, itemID, itemURL);
		postMessage(message);
		postUserMessage(cmItemStatus, userMessage);
	}
	
	public void postItemDeleted() throws Exception {
		this.cmItemStatus = null;
		String fullName = ckanUser.getNameSurname();
		String message = String.format(
				"@**%s** deleted the item with name '%s' (id='%s')",
				fullName, itemName, itemID, itemURL);
		postMessage(message);
	}
}
