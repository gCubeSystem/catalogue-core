package org.gcube.gcat.moderation.thread.social.notifications;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.api.moderation.Moderated;
import org.gcube.gcat.moderation.thread.ModerationThread;
import org.gcube.gcat.persistence.ckan.CKANUser;
import org.gcube.gcat.social.SocialUsers;
import org.gcube.gcat.utils.Constants;
import org.gcube.social_networking.social_networking_client_library.NotificationClient;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEvent;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SocialNotificationModerationThread extends ModerationThread {

	private static final Logger logger = LoggerFactory.getLogger(SocialNotificationModerationThread.class);
	
	public static final String AUTHOR = "Author";
	
	protected CatalogueEventType catalogueEventType;
	protected boolean comment;
	
	protected boolean notificationSentByGCat;
	protected boolean notificationToSelfOnly;
	
	public SocialNotificationModerationThread() {
		super();
		this.comment = false;
		this.notificationSentByGCat = false;
		this.notificationToSelfOnly = false;
	}
	
	/**
	 * Create the message for an item that is created/updated
	 */
	protected void notifyItemToBeManaged() throws Exception {
		/* 
		 * An example of created message is:
		 * 
		 * [mister x] created/updated the item "[TITLE]". You are kindly requested to review it and decide either to APPROVE or REJECT it. [Go to catalogue]
		 * 
		 */
		String fullName = ckanUser.getNameSurname();
		StringBuffer stringBuffer = new StringBuffer();
		if(notificationSentByGCat) {
			stringBuffer.append(fullName);
		}
		stringBuffer.append(create ? " created " : " updated ");
		stringBuffer.append("the item ");
		stringBuffer = addQuotedTitle(stringBuffer);
		stringBuffer.append(". You are kindly requested to review it and decide either to APPROVE or REJECT it. ");
		postMessage(stringBuffer.toString());
		
		
		notificationToSelfOnly = true;
		notificationSentByGCat = true;
		stringBuffer = new StringBuffer();
		stringBuffer.append("Thank you for submitting your item ");
		stringBuffer = addQuotedTitle(stringBuffer);
		stringBuffer.append(" to the catalogue. This item has been successfully received and will be managed by the catalogue moderators.");
		postMessage(stringBuffer.toString());
		notificationToSelfOnly = false;
		notificationSentByGCat = false;
	}
	
	protected void notifyItemDeleted() throws Exception {
		String fullName = ckanUser.getNameSurname();
		StringBuffer stringBuffer = new StringBuffer();
		if(notificationSentByGCat) {
			stringBuffer.append(fullName);
		}
		stringBuffer.append(" permanently deleted ");
		stringBuffer.append("the item ");
		stringBuffer = addQuotedTitle(stringBuffer);
		stringBuffer.append(".");
		postMessage(stringBuffer.toString());
	}
	
	@Override
	public void postItemCreated() throws Exception {
		create = true;
		cmItemStatus = CMItemStatus.PENDING;
		catalogueEventType = CatalogueEventType.ITEM_SUBMITTED;
		notifyItemToBeManaged();
	}
	
	@Override
	public void postItemUpdated() throws Exception {
		create = false;
		cmItemStatus = CMItemStatus.PENDING;
		catalogueEventType = CatalogueEventType.ITEM_UPDATED;
		notifyItemToBeManaged();
	}
	
	protected StringBuffer addUserWithRole(String fullName, String role, StringBuffer stringBuffer, boolean addUserFullName) {
		if(addUserFullName) {
			stringBuffer.append(fullName);
		}
		if(role!=null) {
			stringBuffer.append(" [");
			stringBuffer.append(role);
			stringBuffer.append("] ");
		}
		return stringBuffer;
	}
	
	protected StringBuffer addUserWithRole(String fullName, String role, StringBuffer stringBuffer) {
		return addUserWithRole(fullName, role, stringBuffer, notificationSentByGCat);
	}
	
	public void postItemManaged(String userMessage) throws Exception {
		/*
		 * [mister x] rejected the item "[TITLE]" with this accompanying message "[MESSAGE]". To resubmit it [Go to catalogue]
		 * 
		 * [mister x] approved the item "[TITLE]" with this accompanying message "[MESSAGE]". [Go to catalogue]
		 */
		this.create = false;
		String fullName = ckanUser.getNameSurname();
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = addUserWithRole(fullName, Moderated.CATALOGUE_MODERATOR, stringBuffer);
		stringBuffer.append(cmItemStatus.getValue());
		stringBuffer.append(" the item ");
		stringBuffer = addQuotedTitle(stringBuffer);
		if(userMessage!=null && userMessage.length()>0) {
			stringBuffer.append(" with this accompanying message \"");
			stringBuffer.append(userMessage);
			stringBuffer.append("\"");
		}
		stringBuffer.append(".");
		
		if(cmItemStatus == CMItemStatus.REJECTED) {
			stringBuffer.append(" To resubmit it ");
		}
		postMessage(stringBuffer.toString());
	}

	@Override
	public void postItemRejected(String userMessage) throws Exception {
		this.create = false;
		this.cmItemStatus = CMItemStatus.REJECTED;
		this.catalogueEventType = CatalogueEventType.ITEM_REJECTED;
		postItemManaged(userMessage);
	}
	
	@Override
	public void postItemApproved(String userMessage) throws Exception {
		this.create = false;
		this.cmItemStatus = CMItemStatus.APPROVED;
		this.catalogueEventType = CatalogueEventType.ITEM_PUBLISHED;
		postItemManaged(userMessage);
	}
	
	@Override
	public void postItemDeleted() throws Exception {
		this.create = false;
		this.cmItemStatus = null;
		this.catalogueEventType = CatalogueEventType.ITEM_REMOVED;
		notifyItemDeleted();
	}
	
	protected StringBuffer addQuotedTitle(StringBuffer stringBuffer, String quotingCharacter) {
		stringBuffer.append(quotingCharacter);
		stringBuffer.append(itemTitle);
		stringBuffer.append(quotingCharacter);
		return stringBuffer;
	}
	
	protected StringBuffer addQuotedTitle(StringBuffer stringBuffer) {
		return addQuotedTitle(stringBuffer, "\"");
	}
	
	protected String getSubject() {
		StringBuffer stringBuffer = new StringBuffer();
		String fullName = ckanUser.getNameSurname();
		if(!comment) {
			switch (catalogueEventType) {
				case ITEM_SUBMITTED:
					stringBuffer.append(fullName);
					stringBuffer.append(" created the item ");
					break;
		
				case ITEM_UPDATED:
					stringBuffer.append(fullName);
					stringBuffer.append(" updated the item ");
					break;
				
				case ITEM_REJECTED:
				case ITEM_PUBLISHED:
					addUserWithRole(fullName, Moderated.CATALOGUE_MODERATOR, stringBuffer, true);
					stringBuffer.append(cmItemStatus.getValue());
					stringBuffer.append(" the item ");
					break;
					
				default:
					break;
			}
		}else {
			addUserWithRole(fullName, itemAuthor ? SocialNotificationModerationThread.AUTHOR : Moderated.CATALOGUE_MODERATOR, stringBuffer, true);
			stringBuffer.append("commented on the item ");
		}
		stringBuffer = addQuotedTitle(stringBuffer);
		return stringBuffer.toString(); 
	}
	
	protected CatalogueEvent getCatalogueEvent(String messageString) throws Exception {
		CatalogueEvent catalogueEvent = new CatalogueEvent();
		catalogueEvent.setType(catalogueEventType);
		catalogueEvent.setNotifyText(messageString);
		catalogueEvent.setItemId(getSubject());
		if(cmItemStatus!=null && cmItemStatus == CMItemStatus.APPROVED) {
			catalogueEvent.setItemURL(new URL(itemURL));
		}else {
			catalogueEvent.setItemURL(new URL(getModerationURL()));
		}
		
		Set<String> users = new HashSet<>();
		
		if(!notificationToSelfOnly) {
			users.addAll(SocialUsers.getUsernamesByRole(Moderated.CATALOGUE_MODERATOR));
			
			if(itemAuthorCkanUsername!=null) {
				// Adding item author
				users.add(CKANUser.getUsernameFromCKANUsername(itemAuthorCkanUsername));
			}
		}
		
		// Adding current user
		users.add(CKANUser.getUsernameFromCKANUsername(ckanUser.getName()));
		
		catalogueEvent.setIdsToNotify(users.toArray(new String[users.size()]));
		catalogueEvent.setIdsAsGroup(false);
		
		return catalogueEvent;
	}
	
	@Override
	protected void postMessage(String messageString) throws Exception {
		CatalogueEvent catalogueEvent = getCatalogueEvent(messageString);
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		if(notificationSentByGCat) {
			secretManager.startSession(secret);
		}
		try {
			sendNotification(catalogueEvent);
		}finally {
			if(notificationSentByGCat) {
				secretManager.endSession();
			}
		}
	}

	@Override
	public void postUserMessage(CMItemStatus cmItemStatus, String userMessage) throws Exception {
		/*
		 * [mister x] ([Role]) commented on the item "[TITLE]" as follows "[MESSAGE]". [Go to catalogue]
		 */
		this.create = false;
		this.cmItemStatus = cmItemStatus;
		this.comment = true;
		
		switch (cmItemStatus) {
			case PENDING:
				catalogueEventType = CatalogueEventType.ITEM_UPDATED;
				break;
				
			case APPROVED:
				catalogueEventType = CatalogueEventType.ITEM_PUBLISHED;
				break;
				
			case REJECTED:
				catalogueEventType = CatalogueEventType.ITEM_REJECTED;
				break;
			
			default:
				break;
		}
		
		String fullName = ckanUser.getNameSurname();
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = addUserWithRole(fullName, itemAuthor ? SocialNotificationModerationThread.AUTHOR : Moderated.CATALOGUE_MODERATOR, stringBuffer);
		stringBuffer.append("commented on the item ");
		stringBuffer = addQuotedTitle(stringBuffer);
		stringBuffer.append(" as follows \"");
		stringBuffer.append(userMessage);
		stringBuffer.append("\".");
		CatalogueEvent catalogueEvent = getCatalogueEvent(stringBuffer.toString());
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		if(notificationSentByGCat) {
			secretManager.startSession(secret);
		}
		try {
			sendNotification(catalogueEvent);
		}finally {
			if(notificationSentByGCat) {
				secretManager.endSession();
			}
		}
	}

	protected void sendNotification(CatalogueEvent catalogueEvent) throws Exception {
		Thread thread = new Thread() {
		    public void run() {
		        try {
		        	logger.trace("{} is going to send the following notification {}", SecretManagerProvider.instance.get().getUser().getUsername(), catalogueEvent);
		        	NotificationClient nc = new NotificationClient();
		    		nc.sendCatalogueEvent(catalogueEvent);
		        } catch(Exception e) {
		            logger.error("Error while sending notification.", e);
		        }
		    }  
		};
		// thread.run();
		thread.start();
	}

	@Override
	protected void createModerationThread() throws Exception {
		create = true;
		cmItemStatus = CMItemStatus.PENDING;
	}
	
}
