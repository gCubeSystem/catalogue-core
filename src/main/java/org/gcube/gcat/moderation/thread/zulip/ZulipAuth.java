package org.gcube.gcat.moderation.thread.zulip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ZulipAuth {

	public static final String ZULIP_RC_FILENAME = "zuliprc";
	
	public static final String EMAIL_KEY = "email";
	public static final String KEY_KEY = "key";
	public static final String SITE_KEY = "site";
	
	protected final Properties properties;
	
	public ZulipAuth(String username) {
		properties = new Properties();
		InputStream input = ZulipAuth.class.getClassLoader().getResourceAsStream(username+"_"+ZULIP_RC_FILENAME);
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getEmail() {
		return properties.getProperty(EMAIL_KEY);
	}
	
	public String getAPIKey() {
		return properties.getProperty(KEY_KEY);
	}
	
	public String getSite() {
		return properties.getProperty(SITE_KEY);
	}
	
}
