package org.gcube.gcat.moderation.thread.zulip;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ZulipResponse {
	
	public static final String RESULT_KEY = "result";
	public static final String MSG_KEY = "msg";
	
	public enum Result {
		success,
		error
	}
	
	protected ObjectMapper objectMapper;
	
	protected String responseString;
	protected JsonNode response;
	
	protected Result result;
	protected String message; 
	
	public ZulipResponse(String responseString) {
		this.responseString = responseString;
		this.objectMapper = new ObjectMapper();
	}
	
	public Result getResponseResult() throws JsonProcessingException, IOException {
		if(result==null) {
			String resultString = getResponse().get(RESULT_KEY).asText();
			result = Result.valueOf(resultString);
		}
		return result;
	}
	
	public String getResponseMessage() throws JsonProcessingException, IOException {
		if(message==null) {
			message = getResponse().get(MSG_KEY).asText();
		}
		return message;
	}
	
	public JsonNode getResponse() throws JsonProcessingException, IOException {
		if(response == null) {
			response = objectMapper.readTree(responseString);
		}
		return response;
	}
	
}
