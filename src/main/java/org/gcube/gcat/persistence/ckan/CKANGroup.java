package org.gcube.gcat.persistence.ckan;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANGroup extends CKAN {
	
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.get.group_list
	public static final String GROUP_LIST = CKAN.CKAN_API_PATH + "group_list";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.create.group_create
	public static final String GROUP_CREATE = CKAN.CKAN_API_PATH + "group_create";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.get.group_show
	public static final String GROUP_SHOW = CKAN.CKAN_API_PATH + "group_show";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.update.group_update
	public static final String GROUP_UPDATE = CKAN.CKAN_API_PATH + "group_update";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.patch.group_patch
	public static final String GROUP_PATCH = CKAN.CKAN_API_PATH + "group_patch";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.delete.group_delete
	public static final String GROUP_DELETE = CKAN.CKAN_API_PATH + "group_delete";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.delete.group_purge
	public static final String GROUP_PURGE = CKAN.CKAN_API_PATH + "group_purge";
	
	public static final String GROUPS_KEY = "groups";
	
	protected String title;
	
	public void setTitle(String title) {
		this.title = title;
	}

	public CKANGroup() {
		super();
		LIST = GROUP_LIST;
		CREATE = GROUP_CREATE;
		READ = GROUP_SHOW;
		UPDATE = GROUP_UPDATE;
		PATCH = GROUP_PATCH;
		DELETE = GROUP_DELETE;
		PURGE = GROUP_PURGE;
	}
	
	public static String fromGroupTitleToName(String groupTitle) {
		if(groupTitle == null) {
			return null;
		}
		
		String regexGroupTitleTransform = "[^A-Za-z0-9-]";
		String modified = groupTitle.trim().replaceAll(regexGroupTitleTransform, "-").replaceAll("-+", "-").toLowerCase();
		
		if(modified.startsWith("-"))
			modified = modified.substring(1);
		if(modified.endsWith("-"))
			modified = modified.substring(0, modified.length() - 1);
		
		return modified;
		
	}
	
	public static String getCKANGroupName(String name) {
		return CKANGroup.fromGroupTitleToName(name);
	}
	
	public String create() throws WebApplicationException {
		try {
			ObjectNode objectNode = mapper.createObjectNode();
			objectNode.put(NAME_KEY, CKANGroup.getCKANGroupName(name));
			if(title==null || title.compareTo("")==0) {
				title = name;
			}
			objectNode.put("title", title);
			objectNode.put("display_name", title);
			objectNode.put("description", "");
			return super.create(mapper.writeValueAsString(objectNode));
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public List<String> getGroups() {
		if(result == null) {
			read();
		}
		List<String> groups = new ArrayList<String>();
		if(result.has(GROUPS_KEY)) {
			JsonNode jsonNode = result.get(GROUPS_KEY);
			if(jsonNode.isArray()) {
				ArrayNode arrayNode = (ArrayNode) jsonNode;
				if(arrayNode.size() > 0) {
					Iterator<JsonNode> iterator = arrayNode.iterator();
					while(iterator.hasNext()) {
						groups.add(iterator.next().asText());
					}
				}
			}
		}
		return groups;
	}
	
	public int count() {
		list(100000, 0);
		ArrayNode arrayNode = (ArrayNode) result;
		return arrayNode.size();
	}
	
}
