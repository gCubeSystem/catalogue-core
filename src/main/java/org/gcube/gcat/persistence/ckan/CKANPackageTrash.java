package org.gcube.gcat.persistence.ckan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.WebApplicationException;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.gcat.api.configuration.CKANDB;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;
import org.postgresql.core.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANPackageTrash {
	
	protected static final Logger logger = LoggerFactory.getLogger(CKANPackageTrash.class);
	
	private static final String GROUP_TABLE_KEY = "group";
	private static final String GROUP_ID_KEY = "id";
	private static final String GROUP_NAME_KEY = "name";
	
	private static final String PACKAGE_TABLE_KEY = "package";
	
	private static final String PACKAGE_NAME_KEY = "name";
	
	private static final String PACKAGE_TYPE_KEY = "type";
	private static final String PACKAGE_TYPE_VALUE = "dataset";
	
	private static final String PACKAGE_STATE_KEY = "state";
	private static final String PACKAGE_STATE_VALUE = "deleted";
	
	private static final String PACKAGE_OWNER_ORG_KEY = "owner_org";
	
	protected ObjectMapper mapper;
	
	protected final CKANUser ckanUser;
	protected final CatalogueConfiguration configuration;
	protected final Set<String> supportedOrganizations;
	
	protected boolean ownOnly; 
	
	public CKANPackageTrash() {
		this(CatalogueConfigurationFactory.getInstance());
	}
	
	public CKANPackageTrash(CatalogueConfiguration configuration) {
		this.mapper = new ObjectMapper();
		this.ckanUser = CKANUserCache.getCurrrentCKANUser();
		this.configuration = configuration;
		this.supportedOrganizations = configuration.getSupportedOrganizations();
		this.ownOnly = true;
	}
	
	public void setOwnOnly(boolean ownOnly) {
		this.ownOnly = ownOnly;
	}
	
	protected Connection getConnection() throws Exception {
		Class.forName("org.postgresql.Driver");
		CKANDB ckanDB = configuration.getCkanDB();
		String url = ckanDB.getUrl();
		String username = ckanDB.getUsername();
		String password = ckanDB.getPassword();
		Connection connection = DriverManager.getConnection(url, username, password);
		logger.trace("Database {} opened successfully", url);
		connection.setAutoCommit(false);
		return connection;
	}
	
	protected String getQuotedString(String string) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		Utils.escapeLiteral(builder, string, false);
		builder.append("'");
		return builder.toString();
	}
		
	protected ArrayNode getItems() throws WebApplicationException {
		Connection connection = null;
		try {
			StringBuffer stringBufferOrg = new StringBuffer();
			stringBufferOrg.append("SELECT ");
			stringBufferOrg.append(GROUP_ID_KEY);
			stringBufferOrg.append(" FROM \"");
			stringBufferOrg.append(GROUP_TABLE_KEY);
			stringBufferOrg.append("\" WHERE ");
			stringBufferOrg.append(GROUP_NAME_KEY);
			stringBufferOrg.append(" IN ");
			stringBufferOrg.append("(");
			boolean first = true;
			for(String organizationName : supportedOrganizations) {
				if(first) {
					first = false;
				}else {
					stringBufferOrg.append(",");
				}
				if(organizationName.compareTo("grsf")==0) {
					stringBufferOrg.append(getQuotedString("grsf_pre"));
				}else {
				stringBufferOrg.append(getQuotedString(organizationName));
				}
			}
			stringBufferOrg.append(")");
			
			
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append("SELECT ");
			stringBuffer.append(PACKAGE_NAME_KEY);
			stringBuffer.append(" FROM ");
			stringBuffer.append(PACKAGE_TABLE_KEY);
			stringBuffer.append(" WHERE ");
			stringBuffer.append(PACKAGE_TYPE_KEY);
			stringBuffer.append("=");
			stringBuffer.append(getQuotedString(PACKAGE_TYPE_VALUE));
			stringBuffer.append(" AND ");
			stringBuffer.append(PACKAGE_STATE_KEY);
			stringBuffer.append("=");
			stringBuffer.append(getQuotedString(PACKAGE_STATE_VALUE));
			
			if(ownOnly || ckanUser.getRole().ordinal()<Role.ADMIN.ordinal()) {
				// add only own items
				stringBuffer.append(" AND ");
				stringBuffer.append(CKANPackage.AUTHOR_EMAIL_KEY);
				stringBuffer.append("=");
				stringBuffer.append(getQuotedString(ckanUser.getEMail()));
			}
			
			stringBuffer.append(" AND ");
			stringBuffer.append(PACKAGE_OWNER_ORG_KEY);
			stringBuffer.append(" IN (");
			stringBuffer.append(stringBufferOrg);
			stringBuffer.append(")");
			
			ArrayNode items = mapper.createArrayNode();
			connection = getConnection();
			
			Statement statement = connection.createStatement();
		
			String sql = stringBuffer.toString();
			logger.trace("Going to request the following query: {}", sql);
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				String id = resultSet.getString(PACKAGE_NAME_KEY);
				items.add(id);
			}
			
			return items;
		} catch (WebApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					
				}
			}
		}
	}
	
	public String list() throws WebApplicationException {
		try {
			return mapper.writeValueAsString(getItems());
		} catch (WebApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
	public ObjectNode removeAll() throws WebApplicationException {
		ObjectNode objectNode = mapper.createObjectNode();
		ArrayNode deleted = mapper.createArrayNode();
		ArrayNode notDeleted = mapper.createArrayNode();
		
		ArrayNode itemNames = getItems();
		CKANPackage ckanPackage = new CKANPackage();
		for(int i=0; i<itemNames.size(); i++) {
			String name = itemNames.get(i).asText();
			try {
				ckanPackage.reuseInstance();
				ckanPackage.setName(name);
				ckanPackage.purge();
				deleted.add(name);
			}catch (Exception e) {
				notDeleted.add(name);
			}
			try {
				Thread.sleep(TimeUnit.MILLISECONDS.toMillis(100));
			} catch (InterruptedException e) {
				
			}
		}
		
		objectNode.set("deleted", deleted);
		objectNode.set("failed", notDeleted);
		return objectNode;
	}
	
	public String empty() throws WebApplicationException {
		try {
			return mapper.writeValueAsString(removeAll());
		} catch (WebApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

}
