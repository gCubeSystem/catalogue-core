package org.gcube.gcat.persistence.ckan;

import java.util.Collection;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.user.User;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.moderation.Moderated;
import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.utils.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANUser extends CKAN {
	
	private static final Logger logger = LoggerFactory.getLogger(CKANUser.class);
	
	/* User Paths */
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.get.user_list
	public static final String USER_LIST = CKAN.CKAN_API_PATH + "user_list";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.create.user_create
	public static final String USER_CREATE = CKAN.CKAN_API_PATH + "user_create";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.get.user_show
	public static final String USER_SHOW = CKAN.CKAN_API_PATH + "user_show";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.update.user_update
	public static final String USER_UPDATE = CKAN.CKAN_API_PATH + "user_update";
	// see https://docs.ckan.org/en/latest/api/#ckan.logic.action.delete.user_delete
	public static final String USER_DELETE = CKAN.CKAN_API_PATH + "user_delete";
	
	public static final String ADD_USER_TO_GROUP = CKAN.CKAN_API_PATH + "member_create";
	
	public static final String NAME = "name";
	public static final String DISPLAY_NAME = "display_name";
	public static final String FULL_NAME = "fullname";
	public static final String ABOUT = "about";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	
	private static final String API_KEY = "apikey";
	
	public static final String PORTAL_ROLES = "portal_roles";
	
	protected String nameSurname;
	protected Role role;
	protected Boolean catalogueModerator;
	
	protected CatalogueConfiguration configuration;
	
	public CKANUser() {
		this(CatalogueConfigurationFactory.getInstance());
	}
	
	protected CKANUser(CatalogueConfiguration configuration) {
		super();
		this.LIST = USER_LIST;
		this.CREATE = USER_CREATE;
		this.READ = USER_SHOW;
		this.UPDATE = USER_UPDATE;
		this.PATCH = null;
		this.DELETE = USER_DELETE;
		this.PURGE = null;
		this.catalogueModerator = null;
		this.configuration = configuration;
	}
	
	public void setName(String name) {
		name = getCKANUsername(name);
		this.name = name;
	}
	
	public String createInCkan() {
		RandomString randomString = new RandomString(12);
		ObjectNode objectNode = mapper.createObjectNode();
		objectNode.put(NAME, name);
		objectNode.put(PASSWORD, randomString.nextString());
		checkAndSetEmail(objectNode);
		checkAndSetFullName(objectNode);
		checkAndSetJobTitle(objectNode);
		return create(getAsString(objectNode));
	}
	
	@Override
	public void delete(boolean purge) {
		this.delete();
	}
	
	/**
	 * 
	 * @param objectNode
	 * @return true if the display name and the full name has been updated in objectNode
	 */
	private boolean checkAndSetJobTitle(ObjectNode objectNode) {
		String jobTitle = SecretManagerProvider.instance.get().getUser().getAbout();
		
		String ckanJobTitle = "";
		if(objectNode.has(ABOUT)) {
			ckanJobTitle = objectNode.get(ABOUT).asText();
		}
		if(jobTitle!=null && jobTitle.compareTo(ckanJobTitle) != 0) {
			objectNode.put(ABOUT, jobTitle);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param objectNode
	 * @return true if the display name and the full name has been updated in objectNode
	 */
	private boolean checkAndSetFullName(ObjectNode objectNode) {
		User user = SecretManagerProvider.instance.get().getUser();
		String portalFullname = user.getFullName();
		this.nameSurname = user.getFullName(true);
		
		String ckanFullname = "";
		if(objectNode.has(FULL_NAME)) {
			ckanFullname = objectNode.get(FULL_NAME).asText();
		}
		if(portalFullname!=null && portalFullname.compareTo(ckanFullname) != 0) {
			objectNode.put(FULL_NAME, portalFullname);
			objectNode.put(DISPLAY_NAME, portalFullname);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param objectNode
	 * @return true if the display name and the full name has been updated
	 */
	private boolean checkAndSetEmail(ObjectNode objectNode) {
		User user = SecretManagerProvider.instance.get().getUser();
		String portalEmail = user.getEmail();
		
		String ckanEmail = "";
		if(objectNode.has(EMAIL)) {
			ckanEmail = objectNode.get(EMAIL).asText();
		}
		if(portalEmail==null) {
			String username = user.getUsername();
			String eMail = username + "@d4science.org";
			objectNode.put(EMAIL, eMail);
			return true;
		} else if(portalEmail.compareTo(ckanEmail) != 0) {
			objectNode.put(EMAIL, portalEmail);
			return true;
		}
		return false;
	}
	
	/**
	 * Update the user profile on CKAN if the got got informations differs from the portal information
	 * @return true if the profile information has been updated
	 */
	protected boolean updateProfileIfNeeded() {
		ObjectNode objectNode = (ObjectNode) result;
		boolean toBeUpdated = false;
		
		toBeUpdated = checkAndSetEmail(objectNode) || toBeUpdated;
		toBeUpdated = checkAndSetFullName(objectNode) || toBeUpdated;
		toBeUpdated = checkAndSetJobTitle(objectNode) || toBeUpdated;
		
		if(toBeUpdated) {
			update(getAsString(objectNode));
		}
		return toBeUpdated;
	}
	
	protected void addUserToSupportedOrganizations() {
		try {
			CatalogueConfiguration configuration = CatalogueConfigurationFactory.getInstance();
			for(String supportedOrganization : configuration.getSupportedOrganizations()) {
				addUserToOrganization(supportedOrganization);
			}
		}catch (Exception e) {
			// The organization could not exists and this is fine in some cases like organization create or 
			// for listing items at VO level. The organization corresponding to the VO could not exists.
			logger.warn("Add user to organization {} failed. This is acceptable in the case the request is at VO level and the corresponding orgnization does not esists and should not, as well as when the organization is going to be created", CKANOrganization.getCKANOrganizationName());
		}
	}
	
	
	public void retrieve() {
		setApiKey(CKANUtility.getSysAdminAPI());
		try {
			if(name == null || name.compareTo("") == 0) {
				setName(getCKANUsername());
			}
			read();
			updateProfileIfNeeded();
		} catch(WebApplicationException e) {
			if(e.getResponse().getStatusInfo() == Status.NOT_FOUND) {
				createInCkan();
			} else {
				throw e;
			}
		}
		addUserToSupportedOrganizations();
	}
	
	protected String parseResult() {
		name = result.get(NAME).asText();
		
		// Only managers can read Ckan API key
		if(getRole().ordinal()<Role.MANAGER.ordinal()) {
			try {
				apiKey = result.get(API_KEY).asText();
			}catch (Exception e) {
				// The user reading its own Ckan profile must be able to read its API key
				if(name.compareTo(getCKANUsername())==0) {
					throw e;
				}
			}
		}else {
			((ObjectNode) result).remove(API_KEY);
		}
		
		if(name.compareTo(getCKANUsername())==0) {
			ArrayNode roles = ((ObjectNode) result).putArray(PORTAL_ROLES);
			roles.add(getRole().getPortalRole());
			if(isCatalogueModerator()) {
				roles.add(Moderated.CATALOGUE_MODERATOR);
			}
		}
		
		return getAsString(result);
	}
	
	protected static String getCKANUsername(String username) {
		if(username == null) {
			return null;
		}
		return username.trim().replaceAll("\\.", "_");
	}
	
	public static String getUsernameFromCKANUsername(String ckanUsername) {
		if(ckanUsername == null) {
			return null;
		}
		return ckanUsername.trim().replaceAll("_", ".");
	}
	
	
	public static String getCKANUsername() {
		String username = SecretManagerProvider.instance.get().getUser().getUsername();
		return getCKANUsername(username);
	}
	
	public String create(String json) {
		super.create(json);
		String ret = parseResult();
		return ret;
	}
	
	public String read() {
		super.read();
		String ret = parseResult();
		return ret;
	}
	
	public String update(String json) {
		super.update(json);
		String ret = parseResult();
		return ret;
	}
	
	public void addUserToOrganization(String organizationName, String ckanUsername, String role) {
		logger.trace("Going to add user {} to organization {} with role {}", ckanUsername, organizationName, role);
		CKANOrganization ckanOrganization = new CKANOrganization();
		ckanOrganization.setApiKey(CKANUtility.getSysAdminAPI());
		ckanOrganization.setName(organizationName);
		ckanOrganization.addUserToOrganisation(ckanUsername, role);
	}
	
	public Role getRole() {
		if(role == null) {
			role = Role.MEMBER;
			Collection<String> roles = SecretManagerProvider.instance.get().getUser().getRoles();
			for(String portalRole : roles) {
				Role gotRole = Role.getRoleFromPortalRole(portalRole);
				if(gotRole != null && gotRole.ordinal() > role.ordinal()) {
					role = gotRole;
				}
			}
		}
		return role;
	}
	
	public void addUserToOrganization(String organizationName) {
		addUserToOrganization(organizationName, name, getRole().getCkanRole());
	}
	
	public void addUserToOrganization() {
		String organizationName = CKANOrganization.getCKANOrganizationName();
		addUserToOrganization(organizationName);
	}
	
	public void addToGroup(String groupName) throws WebApplicationException {
		try {
			ObjectNode objectNode = mapper.createObjectNode();
			objectNode.put(ID_KEY, CKANGroup.getCKANGroupName(groupName));
			objectNode.put("object", name);
			objectNode.put("object_type", "user");
			objectNode.put("capacity", "member");
			sendPostRequest(ADD_USER_TO_GROUP, getAsString(objectNode));
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}

	public boolean isCatalogueModerator() {
		if(catalogueModerator == null) {
			catalogueModerator = SecretManagerProvider.instance.get().getUser().getRoles().contains(Moderated.CATALOGUE_MODERATOR);
		}
		return catalogueModerator;
	}

	public String getSurnameName(){
		return result.get(FULL_NAME).asText();
	}
	
	public String getNameSurname() {
		return nameSurname;
	}
	
	public String getEMail() {
		return result.get(EMAIL).asText();
	}
	
}