package org.gcube.gcat.persistence.ckan;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANUtility {
	
	public static String getCkanURL() {
		try {
			return CatalogueConfigurationFactory.getInstance().getCkanURL();
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public static String getSysAdminAPI() {
		try {
			return CatalogueConfigurationFactory.getInstance().getSysAdminToken();
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public static String getApiKey() {
		try {
			CKANUser ckanUser = CKANUserCache.getCurrrentCKANUser();
			return ckanUser.getApiKey();
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
}
