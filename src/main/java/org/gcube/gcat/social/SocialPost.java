package org.gcube.gcat.social;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.utils.Constants;
import org.gcube.portal.databook.shared.Post;
import org.gcube.social_networking.social_networking_client_library.PostClient;
import org.gcube.social_networking.socialnetworking.model.beans.PostInputBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SocialPost extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(SocialPost.class);
	
	protected static final String NOTIFICATION_MESSAGE = "%s just published the item \"%s\"\n"
			+ "Please find it at %s\n";
	
	protected String userFullName;
	protected String itemID;
	protected String itemURL;
	protected String itemTitle;
	protected List<String> tags;
	protected Boolean notification;
	
	public SocialPost() throws Exception {
		super();
	}
	
	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	public String getItemID() {
		return itemID;
	}
	
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
	
	public String getItemURL() {
		return itemURL;
	}
	
	public void setItemURL(String itemURL) {
		this.itemURL = itemURL;
	}
	
	public String getItemTitle() {
		return itemTitle;
	}
	
	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}
	
	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public void setTags(ArrayNode tags) {
		this.tags = new ArrayList<>();
		if(tags != null && tags.size() > 0) {
			for(int i = 0; i < tags.size(); i++) {
				JsonNode jsonNode = tags.get(i);
				String tagName = "";
				if(jsonNode.has("display_name")) {
					tagName = jsonNode.get("display_name").asText();
				} else {
					tagName = jsonNode.get("name").asText();
				}
				this.tags.add(tagName);
			}
		}
	}
	
	public Boolean isNotification() {
		return notification;
	}

	public void setNotification(Boolean notification) {
		this.notification = notification;
	}
	
	@Override
	public void run() {
		
		try {
			
			CatalogueConfiguration instance = CatalogueConfigurationFactory.getInstance();
			
			if(!instance.isSocialPostEnabled()) {
				logger.info("Social Post are disabled in the context {}", SecretManagerProvider.instance.get().getContext());
				return;
			}
			logger.info("Going to send Social Post about the Item {} available at {}", itemID, itemURL);
			
			boolean notifyUsers = instance.isNotificationToUsersEnabled();
			
			if(notification != null) {
				if(notifyUsers) {
					notifyUsers = notifyUsers && notification;
				}else {
					notifyUsers = notification;
				}
			}
			
			// write notification post
			sendSocialPost(notifyUsers);
			
		} catch(Exception e) {
			logger.error("Error while executing post creation actions", e);
		}
	}
	
	public void sendSocialPost(boolean notifyUsers) {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			StringWriter messageWriter = new StringWriter();
			messageWriter.append(String.format(NOTIFICATION_MESSAGE, userFullName, itemTitle, itemURL));
			
			for(String tag : tags) {
				tag = tag.trim();
				tag = tag.replaceAll(" ", "_").replace("_+", "_");
				if(tag.endsWith("_")) {
					tag = tag.substring(0, tag.length() - 1);
				}
				messageWriter.append("#");
				messageWriter.append(tag);
				messageWriter.append(" ");
			}
			String message = messageWriter.toString();
			
			logger.debug("The social post that is going to be written is\n{}", message);

			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
						
			PostClient postClient = new PostClient();
			PostInputBean postInputBean = new PostInputBean();
			postInputBean.setEnablenotification(notifyUsers);
			postInputBean.setText(message);
			Post post = postClient.writeApplicationPost(postInputBean);
			logger.trace("Sent post {}", post);
			
		} catch(Exception e) {
			logger.error("Unable to send Social Post", e);
		} finally {
			secretManager.endSession();
		}
		
	}

}
