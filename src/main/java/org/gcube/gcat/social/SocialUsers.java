package org.gcube.gcat.social;

import java.util.HashSet;
import java.util.Set;

import org.gcube.social_networking.social_networking_client_library.UserClient;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SocialUsers {

	public static Set<String> getUsernamesByRole(String roleName) throws Exception {
		UserClient userClient = new UserClient();
		Set<String> usernames = new HashSet<>(userClient.getAllUsernamesByRole(roleName));
		return usernames;
	}
	
}
