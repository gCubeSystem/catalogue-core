package org.gcube.gcat.utils;

import java.io.InputStream;
import java.net.URL;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Properties;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.TokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Constants {
	
	private static final Logger logger = LoggerFactory.getLogger(Constants.class);
	
	public static final String CATALOGUE_NAME = "gCat";
	
	protected static final String CLIENT_ID_SECRET_FILENAME = "config.properties";
	protected static final String CLIENT_ID_PROPERTY_NAME = "clientId";
	
	private static Entry<String, String> getClientIdAndClientSecret(String context) {
		try {
			Properties properties = new Properties();
			ClassLoader classLoader = Constants.class.getClassLoader();
			URL url = classLoader.getResource(CLIENT_ID_SECRET_FILENAME);
			logger.trace("Going to read {} at {}", CLIENT_ID_SECRET_FILENAME, url.toString());
			InputStream input = classLoader.getResourceAsStream(CLIENT_ID_SECRET_FILENAME);
			properties.load(input);
			
			String clientId = "gcat";
			if(properties.containsKey(CLIENT_ID_PROPERTY_NAME)) {
				clientId = properties.getProperty(CLIENT_ID_PROPERTY_NAME);
			}
			
			int index = context.indexOf('/', 1);
			String root = context.substring(0, index == -1 ? context.length() : index);
			String clientSecret = properties.getProperty(root);
			
			SimpleEntry<String, String> entry = new SimpleEntry<String, String>(clientId, clientSecret);
			return entry;
		} catch(Exception e) {
			throw new InternalServerErrorException(
					"Unable to retrieve Application Token for context " + SecretManagerProvider.instance.get().getContext(), e);
		}
	}
	
	private static TokenResponse getJWTAccessToken() throws Exception {
		String context = SecretManagerProvider.instance.get().getContext();
		Entry<String,String> entry = getClientIdAndClientSecret(context);
		TokenResponse tr = KeycloakClientFactory.newInstance().queryUMAToken(context, entry.getKey(), entry.getValue(), context, null);
	    return tr;
	}
	
	public static Secret getCatalogueSecret() throws Exception {
		TokenResponse tr = getJWTAccessToken();
		Secret secret = new JWTSecret(tr.getAccessToken());
		return secret;
	}
	
}
