package org.gcube.gcat.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class HTTPUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(HTTPUtility.class);
	
	public static StringBuilder getStringBuilder(InputStream inputStream) throws IOException {
		StringBuilder result = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while((line = reader.readLine()) != null) {
				result.append(line);
			}
		}
		
		return result;
	}
	
	public static GXHTTPStringRequest createGXHTTPStringRequest(String url, String path, boolean post)
			throws UnsupportedEncodingException {
		GXHTTPStringRequest gxhttpStringRequest = GXHTTPStringRequest.newRequest(url);
		gxhttpStringRequest.from(Constants.CATALOGUE_NAME);
		if(post) {
			gxhttpStringRequest.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
		}
		gxhttpStringRequest.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
		gxhttpStringRequest.path(path);
		return gxhttpStringRequest;
	}
	
	public static String getResultAsString(HttpURLConnection httpURLConnection) throws IOException {
		int responseCode = httpURLConnection.getResponseCode();
		if(responseCode >= Status.BAD_REQUEST.getStatusCode()) {
			Status status = Status.fromStatusCode(responseCode);
			InputStream inputStream = httpURLConnection.getErrorStream();
			StringBuilder result = getStringBuilder(inputStream);
			logger.trace(result.toString());
			throw new WebApplicationException(result.toString(), status);
		}
		InputStream inputStream = httpURLConnection.getInputStream();
		String ret = getStringBuilder(inputStream).toString();
		logger.trace("Got Respose is {}", ret);
		return ret;
	}
	
}
