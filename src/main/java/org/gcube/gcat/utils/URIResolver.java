package org.gcube.gcat.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class URIResolver {
	
	private static final String CATALOGUE_CONTEXT = "gcube_scope";
	private static final String ENTITY_TYPE = "entity_context";
	private static final String ENTITY_NAME = "entity_name";
	
	private static final String DATASET = "dataset";
	
	protected static URIResolver uriResolver;
		
	protected UriResolverManager uriResolverManager;
	protected Calendar expireTime;
	
	public static URIResolver getInstance() {
		if(uriResolver == null) {
			uriResolver = new URIResolver();
		}else {
			Calendar now = Calendar.getInstance();
			if(now.after(uriResolver.expireTime)) {
				uriResolver = new URIResolver();
			}
		}
		return uriResolver;
	}
	
	private URIResolver() {
		try {
			uriResolverManager = new UriResolverManager("CTLG");
			expireTime = Calendar.getInstance();
			expireTime.add(Calendar.MINUTE, 30);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
	}
	
	public String getCatalogueItemURL(String name) {
		try {
			String context = SecretManagerProvider.instance.get().getContext();
			Map<String, String> params = new HashMap<>();
			params.put(CATALOGUE_CONTEXT, context);
			params.put(ENTITY_TYPE, DATASET);
			params.put(ENTITY_NAME, name);
			String url = uriResolverManager.getLink(params, false);
			return url;
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
}