package org.gcube.gcat.workspace;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.gcat.utils.Constants;
import org.gcube.storagehub.MetadataMatcher;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GcatMetadataMatcher extends MetadataMatcher {
	
	public static final String GCAT_METADATA_VERSION = "2.0.0";
	
	public static final String CATALOGUE_ITEM_ID = "CatalogueItemID";
	
	public GcatMetadataMatcher(String id) {
		super(Constants.CATALOGUE_NAME, GCAT_METADATA_VERSION, id);
	}
	
	@Override
	public boolean check(Metadata metadata) {
		Map<String,Object> map = metadata.getMap();
		Object obj = map.get(CATALOGUE_ITEM_ID);
		if(obj!=null && obj.toString().compareTo(id) == 0) {
			return true;
		}
		return false;
	}
	
	@Override
	protected Map<String, Object> getSpecificMetadataMap() {
		Map<String,Object> map = new HashMap<>();
		map.put(CATALOGUE_ITEM_ID, id);
		return map;
	}
	
}
