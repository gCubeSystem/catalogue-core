package org.gcube.gcat.workspace;

import java.net.HttpURLConnection;
import java.net.URL;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.gcat.utils.Constants;
import org.gcube.storagehub.MetadataMatcher;
import org.gcube.storagehub.StorageHubManagement;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GcatStorageHubManagement {
	
	private static final Logger logger = LoggerFactory.getLogger(GcatStorageHubManagement.class);
	
	protected StorageHubManagement storageHubManagement;
	
	protected String itemId;
	protected String originalFilename;
	protected String mimeType;
	
	public String getOriginalFilename() {
		return originalFilename;
	}
	
	public String getMimeType() {
		return mimeType;
	}
	
	public GcatStorageHubManagement() {
		this.storageHubManagement = new StorageHubManagement();
	}
	
	protected MetadataMatcher getMetadataMatcher() {
		MetadataMatcher metadataMatcher = new GcatMetadataMatcher(itemId);
		return metadataMatcher;
	}
	
	protected String getOriginalFileName(HttpURLConnection httpURLConnection) throws Exception {
		String contentDisposition = httpURLConnection.getHeaderField("Content-Disposition");
		contentDisposition = contentDisposition.replaceAll("= ", "=").replaceAll(" =", "=");
		ContentDisposition formDataContentDisposition = new ContentDisposition(contentDisposition);
		return formDataContentDisposition.getFileName();
	}
	
	public URL ensureResourcePersistence(URL persistedURL, String itemID, String resourceID) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		try {
			secretManager.startSession(secret);
			GXHTTPStringRequest gxhttpStringRequest = GXHTTPStringRequest.newRequest(persistedURL.toString());
			gxhttpStringRequest.from(Constants.CATALOGUE_NAME);
			gxhttpStringRequest.isExternalCall(true);
			HttpURLConnection httpURLConnection = gxhttpStringRequest.get();
			mimeType = httpURLConnection.getContentType().split(";")[0];
			originalFilename = getOriginalFileName(httpURLConnection);
			
			this.itemId = itemID;
			storageHubManagement.setMetadataMatcher(getMetadataMatcher());
			
			persistedURL = storageHubManagement.persistFile(httpURLConnection.getInputStream(), resourceID, mimeType);
			return persistedURL;
		} catch (Exception e) {
			logger.error("Error while trying to persists the resource", e);
			throw e;
		} finally {
			secretManager.endSession();
		}
	}
	
	/**
	 * Use {@link GcatStorageHubManagement#deleteResourcePersistence(URL, String)} instead
	 */
	@Deprecated
	public void deleteResourcePersistence(String itemID, String resourceID, String mimeType) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		try {
			secretManager.startSession(secret);
			storageHubManagement = new StorageHubManagement();
			this.itemId = itemID;
			storageHubManagement.setMetadataMatcher(getMetadataMatcher());
			storageHubManagement.removePersistedFile(resourceID, mimeType);
		} catch (Exception e) {
			logger.error("Error while deleting resource with ID {} with mimetype {} , itemID {}", resourceID, mimeType, itemID, e);
		} finally {
			secretManager.endSession();
		}
	}
	
	public void deleteResourcePersistence(URL url, String itemID) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		try {
			secretManager.startSession(secret);
			storageHubManagement = new StorageHubManagement();
			this.itemId = itemID;
			storageHubManagement.setMetadataMatcher(getMetadataMatcher());
			storageHubManagement.removePersistedFile(url);
		} catch (Exception e) {
			logger.error("Error while deleting resource from URL {}, itemID {}", url, itemID, e);
		} finally {
			secretManager.endSession();
		}
	}
	
//	protected void internalAddRevisionID(String resourceID, String revisionID) throws Exception {
//		try {
//			FileContainer fileContainer = null;
//			AbstractFileItem fileItem = null;
//			try {
//				fileContainer = storageHubManagement.getPersistedFile();
//				fileItem = fileContainer.get();
//			}catch (Exception e) {
//				// This is a workaround because storage-hub invalidate the item
//				// when I rename it (just before this operation)
//				// then I get java.lang.RuntimeException: javax.ws.rs.ProcessingException: Error reading entity from input stream.
//				// invoking fileContainer.get()
//				// see issue #25373
//				fileContainer = storageHubManagement.getPersistedFile(resourceID, mimeType);
//				fileItem = fileContainer.get();
//			}
//			Metadata metadata = fileItem.getMetadata();
//			Map<String,Object> map = metadata.getMap();
//			map.put(CatalogueMetadata.CATALOGUE_RESOURCE_ID, resourceID);
//			map.put(CatalogueMetadata.CATALOGUE_RESOURCE_REVISION_ID, revisionID);
//			metadata.setMap(map);
//			fileContainer.setMetadata(metadata);
//		} catch (Exception e) {
//			logger.warn(
//					"Unable to set revision id {} to the file of resource with id {} because the file was NOT found on storage-hub.",
//					revisionID, resourceID);
//			throw e;
//		}
//	}
//	public void renameFile(String resourceID, String revisionID) throws Exception {
	public void renameFile(String resourceID) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		try {
			secretManager.startSession(secret);
			FileContainer createdfile = storageHubManagement.getPersistedFile();
			createdfile.rename(resourceID);
//			internalAddRevisionID(resourceID, revisionID);
		} finally {
			secretManager.endSession();
		}
		
	}
	
//	public void addRevisionID(String resourceID, String revisionID) throws Exception {
//		SecretManager secretManager = SecretManagerProvider.instance.get();
//		Secret secret = Constants.getCatalogueSecret();
//		try {
//			secretManager.startSession(secret);
//			internalAddRevisionID(resourceID, revisionID);
//		} finally {
//			secretManager.endSession();
//		}
//	}
	
	public FileContainer retrievePersistedFile(String filename, String mimeType) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = Constants.getCatalogueSecret();
		try {
			secretManager.startSession(secret);
			return storageHubManagement.getPersistedFile(filename, mimeType);
		} finally {
			secretManager.endSession();
		}
	}
	
	public FileContainer getPersistedFile() throws Exception {
		return storageHubManagement.getPersistedFile();
	}
	
}
