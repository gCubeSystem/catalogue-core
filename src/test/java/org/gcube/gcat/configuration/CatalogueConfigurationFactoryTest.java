package org.gcube.gcat.configuration;

import org.gcube.gcat.ContextTest;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CatalogueConfigurationFactoryTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CatalogueConfigurationFactoryTest.class);
	
	@Test
	public void testConf() throws Exception {
		ServiceCatalogueConfiguration serviceCatalogueConfiguration = CatalogueConfigurationFactory.getInstance();
		logger.debug("{}", serviceCatalogueConfiguration.toJsonString());
		logger.debug("{}", serviceCatalogueConfiguration.toJsonString(true));
	}

}
