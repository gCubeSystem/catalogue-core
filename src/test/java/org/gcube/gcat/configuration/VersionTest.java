package org.gcube.gcat.configuration;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class VersionTest {
	
	private static final Logger logger = LoggerFactory.getLogger(VersionTest.class);
	
	@Test
	public void testVersions() {
		String[] versions = new String[]{"2.2.0-SNAPSHOT", "1.3.5", "1.4.5-beta", "1.5.6-gcore"};
		for(String v : versions) {
			Version version = new Version(v);
			logger.debug("Version is {}", version);
		}
	}
	
}
