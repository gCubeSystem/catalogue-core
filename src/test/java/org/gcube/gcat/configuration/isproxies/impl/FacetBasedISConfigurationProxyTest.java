package org.gcube.gcat.configuration.isproxies.impl;

import java.util.List;

import org.gcube.gcat.ContextTest;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimpleFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.CallsFor;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetBasedISConfigurationProxyTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(FacetBasedISConfigurationProxyTest.class);
	
	@Ignore
	@Test
	public void test() throws Exception {
		FacetBasedISConfigurationProxyFactory facetBasedISConfigurationProxyFactory = new FacetBasedISConfigurationProxyFactory();
		FacetBasedISConfigurationProxy fbiscp = facetBasedISConfigurationProxyFactory.getInstance();
		fbiscp.setServiceEServiceID("f00bbacd-92b8-46d7-b41c-828f71a78753");
		CallsFor<EService, VirtualService> callsFor = fbiscp.createCallsForToVirtualService();
		logger.debug("Created {}", ElementMapper.marshal(callsFor));
		
		SimpleFacet simpleFacet = fbiscp.getISResource();
		logger.debug("{}", ElementMapper.marshal(simpleFacet));
		
		ServiceCatalogueConfiguration catalogueConfiguration = fbiscp.getCatalogueConfiguration();
		logger.debug("{}", catalogueConfiguration.toJsonString());
		logger.debug("{}", catalogueConfiguration.toJsonString(true));
		
		List<CallsFor<EService, VirtualService>> callsForList = fbiscp.deleteCallsForToVirtualService();
		logger.debug("Deleted {} {} relations {}", callsForList.size(), CallsFor.NAME, ElementMapper.marshal(callsForList));
	}
	
}
