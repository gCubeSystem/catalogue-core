package org.gcube.gcat.configuration.isproxies.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.ContextTest;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCoreISConfigurationProxyTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(GCoreISConfigurationProxyTest.class);
	
	// @Test
	public void testGetSupportedOrganizationsFromIS() throws Exception {
		ContextTest.setContextByName("/gcube/devNext/NextNext");
		String context = SecretManagerProvider.instance.get().getContext();
		GCoreISConfigurationProxy gCoreISConfigurationProxy = new GCoreISConfigurationProxy(context);
		@SuppressWarnings("deprecation")
		Set<String> organizations = gCoreISConfigurationProxy.getSupportedOrganizationsFromGenericResource();
		Assert.assertTrue(organizations.size()>0);
	}

	@Test
	public void testCatalogueConfiguration() throws Exception {
		ContextTest.setContextByName("/gcube/devsec/devVRE");
		String context = SecretManagerProvider.instance.get().getContext();
		GCoreISConfigurationProxy gCoreISConfigurationProxy = new GCoreISConfigurationProxy(context);
		ServiceCatalogueConfiguration catalogueConfiguration = gCoreISConfigurationProxy.readFromIS();
		String json = catalogueConfiguration.toJsonString();
		logger.info("Configuration in context {} is {}", context, json);
		/*
		ServiceCatalogueConfiguration secondCatalogueConfiguration = ServiceCatalogueConfiguration.getServiceCatalogueConfiguration(json);
		String secondJson = secondCatalogueConfiguration.toJsonString();
		logger.info("After marshalling and unmarshalling configuration in context {} is {}", context, secondJson);
		String decryptedJson = secondCatalogueConfiguration.toJsonString(true);
		logger.info("Decrypted configuration in context {} is {}", context, decryptedJson);
		ServiceCatalogueConfiguration thirdCatalogueConfiguration = ServiceCatalogueConfiguration.getServiceCatalogueConfiguration(decryptedJson);
		String thirdJson = thirdCatalogueConfiguration.toJsonString();
		logger.info("After marshalling and unmarshalling decrypted configuration in context {} is {}", context, thirdJson);
		logger.info("All as JsonArray [{},{},{},{}]", json, secondJson, decryptedJson, thirdJson);
		*/
	}
	
	// @Test
	public void updateConfigurationToNewVersion() throws Exception {
		ContextTest.setContextByName("/gcube/devsec/devVRE");
		String context = SecretManagerProvider.instance.get().getContext();
		GCoreISConfigurationProxy gCoreISConfigurationProxy = new GCoreISConfigurationProxy(context);
		@SuppressWarnings("deprecation")
		ServiceCatalogueConfiguration catalogueConfiguration = gCoreISConfigurationProxy.getOLDCatalogueConfigurationFromGCoreIS();
		String json = catalogueConfiguration.toJsonString();
		logger.debug("Read configuration {}", json);
		catalogueConfiguration = gCoreISConfigurationProxy.createOrUpdateOnIS();
		json = catalogueConfiguration.toJsonString();
		logger.debug("Updated configuration {}", json);
	}
	
	public static String DEVVRE_CONFIG_JSON = "devvre.conf.json";
	
	//@Test
	public void createConfiguration() throws Exception {
		ContextTest.setContextByName("/gcube/devsec/devVRE");
		String context = SecretManagerProvider.instance.get().getContext();
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(DEVVRE_CONFIG_JSON);
		String json = new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));
		ServiceCatalogueConfiguration catalogueConfiguration = ServiceCatalogueConfiguration.getServiceCatalogueConfiguration(json, ServiceCatalogueConfiguration.class);
		GCoreISConfigurationProxy gCoreISConfigurationProxy = new GCoreISConfigurationProxy(context);
		gCoreISConfigurationProxy.setCatalogueConfiguration(catalogueConfiguration);
		gCoreISConfigurationProxy.createOnIS();
	}
	
	
//	protected GenericResource instantiateGenericResource(String secondaryType, String name, String xml) throws Exception {
//		GenericResource genericResource = new GenericResource();
//		org.gcube.common.resources.gcore.GenericResource.Profile profile = genericResource.newProfile();
//		profile.type(secondaryType);
//		profile.name(name);
//		profile.description("This resource is read by gCat and define the list of CKAN organizations where a client is allowed to publish for the current context");
//		profile.newBody(xml);
//		StringWriter stringWriter = new StringWriter();
//		Resources.marshal(genericResource, stringWriter);
//		logger.debug("The generated {} is\n{}", GenericResource.class.getSimpleName(), stringWriter.toString());
//		return genericResource;
//	}
//	
//	protected void createGenericResource(String xml) throws Exception {
//		GenericResource genericResource = instantiateGenericResource(
//				GCoreISConfigurationProxy.GENERIC_RESOURCE_SECONDARY_TYPE_FOR_ORGANIZATIONS, 
//				GCoreISConfigurationProxy.GENERIC_RESOURCE_NAME_FOR_ORGANIZATIONS, xml);
//		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
//		genericResource = registryPublisher.create(genericResource);
//		StringWriter stringWriter = new StringWriter();
//		Resources.marshal(genericResource, stringWriter);
//		logger.trace("The {} with ID {} has been created \n{}", GenericResource.class.getSimpleName(),
//				genericResource.id(), stringWriter.toString());
//	}
//	
//	protected String createGRBody(List<String> organizations) throws Exception {
//		if(organizations==null || organizations.size()<1) {
//			throw new Exception("Unable to create the body for the generic resource with empty organization list");
//		}
//		ObjectMapper objectMapper = new ObjectMapper();
//		ObjectNode objectNode = objectMapper.createObjectNode();
//		ArrayNode arrayNode = objectNode.putArray(GCoreISConfigurationProxy.GENERIC_RESOURCE_CKAN_ORGANIZATIONS);
//		for(String organizationName : organizations) {
//			arrayNode.add(organizationName);
//		}
//		return objectMapper.writeValueAsString(objectNode);
//	}
//	
//	protected void createGenericResourceForSupportedOrganizations(List<String> organizations) throws Exception {
//		String json = createGRBody(organizations);
//		createGenericResource(json);
//	}
//	
//	// @Test
//	public void createGenericResourceForSupportedOrganizationsByName() throws Exception {
//		List<String> organizations = new ArrayList<>();
//		organizations.add("nextnext");
//		organizations.add("devvre");
//		createGenericResourceForSupportedOrganizations(organizations);
//	}
//	
//	// @Test
//	public void createGenericResourceForSupportedOrganizationsByScopeBean() throws Exception {
//		ContextTest.setContextByName("/gcube/devNext/NextNext");
//		
//		List<ScopeBean> scopeBeans = new ArrayList<>();
//		scopeBeans.add(new ScopeBean("/gcube/devNext/NextNext"));
//		scopeBeans.add(new ScopeBean("/gcube/devsec/devVRE"));
//		List<String> organizations = new ArrayList<>();
//		for(ScopeBean scopeBean : scopeBeans) {
//			organizations.add(CatalogueConfiguration.getOrganizationName(scopeBean));
//		}
//		createGenericResourceForSupportedOrganizations(organizations);
//	}
	
}
