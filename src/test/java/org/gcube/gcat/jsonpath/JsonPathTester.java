package org.gcube.gcat.jsonpath;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.persistence.ckan.CKANPackage;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

public class JsonPathTester {

	private static final Logger logger = LoggerFactory.getLogger(JsonPathTester.class);
	
	public File getExamplesDirectory() throws Exception {
		URL logbackFileURL = JsonPathTester.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "examples");
	}
	
	
	protected String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(file.toPath());
		return new String(encoded, Charset.defaultCharset());
	}
	
	@Test
	public void testJsonPath() throws Exception {
		File examplesDir = getExamplesDirectory();
		File itemFile = new File(examplesDir, "item-01.json");
		String json = readFile(itemFile);
		
		JSONArray systemType = JsonPath.read(json, CKANPackage.JSON_PATH_EXPRESSION_FIND_SYSTEM_TYPE_METADATA);
		Assert.assertTrue(systemType.size()==1);
		logger.trace("System Type : {}", systemType);
		
		String systemTypeString = CKANPackage.getSystemTypeMetadata(json);
		logger.trace("System Type String : {}", systemTypeString);
		
		JSONArray systemMetadata = JsonPath.read(json, CKANPackage.JSON_PATH_EXPRESSION_FIND_SYSTEM_METADATA);
		logger.trace("System Metadata {} ", systemMetadata);
		Map<String, String> systemFieldMap = new HashMap<>();
		Map<String, String> anotherSystemFieldMap = new HashMap<>();
		for(Object obj : systemMetadata) {
			LinkedHashMap<String, String> element = (LinkedHashMap<String, String>) obj;
			systemFieldMap.put(element.get(CKANPackage.EXTRAS_KEY_KEY), element.get(CKANPackage.EXTRAS_VALUE_KEY));
			anotherSystemFieldMap.put(element.get(CKANPackage.EXTRAS_KEY_KEY), element.get(CKANPackage.EXTRAS_VALUE_KEY));
		}
		Assert.assertTrue(systemFieldMap.equals(anotherSystemFieldMap));

		
		JSONArray moderationSystemMetadata = JsonPath.read(json, CKANPackage.JSON_PATH_EXPRESSION_FIND_MODERATION_SYSTEM_METADATA);
		logger.trace("Moderation System Fields {} ", moderationSystemMetadata);
		Assert.assertTrue(moderationSystemMetadata.size()+1 == systemMetadata.size());
		
	}
	
}
