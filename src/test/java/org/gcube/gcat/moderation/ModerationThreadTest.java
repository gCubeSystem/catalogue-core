package org.gcube.gcat.moderation;

import org.gcube.gcat.ContextTest;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.moderation.thread.ModerationThread;
import org.gcube.gcat.persistence.ckan.CKANUser;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ModerationThreadTest extends ContextTest {

	protected ModerationThread getModerationThread(CKANUser ckanUser) {
		ModerationThread moderationThread = ModerationThread.getDefaultInstance();
		moderationThread.setItemCoordinates("b1040e70-774f-47b6-95e9-f24efca50caf", "my_first_restful_transaction_model", "RESTful Transaction Model", "https://data.dev.d4science.org/ctlg/devVRE/my_first_restful_transaction_model");
		moderationThread.setCKANUser(ckanUser);
		return moderationThread;
	}
	
	@Test
	// @Ignore
	public void testModerationThread() throws Exception {
		ContextTest.setContextByName("pasquale.pagano_/gcube/devsec/devVRE");
		
		CKANUser ckanUser = new CKANUser();
		ckanUser.setName(CKANUser.getCKANUsername());
		ckanUser.read();
		
		ModerationThread moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemCreated();
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemUpdated();
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postUserMessage(CMItemStatus.PENDING, "Pensaci Bene");
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemRejected(null);
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemRejected("reject con messaggio: Non mi garba");
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemApproved(null);
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.postItemApproved("approve con messaggio: Ora mi garba");
		
		moderationThread = getModerationThread(ckanUser);
		moderationThread.setItemAuthor(true);
		moderationThread.postUserMessage(CMItemStatus.APPROVED, "Grazie");
		
		Thread.sleep(1000);
	}
	
}
