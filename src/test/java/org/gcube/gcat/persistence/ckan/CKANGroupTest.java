package org.gcube.gcat.persistence.ckan;

import java.util.Arrays;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.ContextTest;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANGroupTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANGroupTest.class);
	
	@Ignore
	@Test
	public void count() throws Exception {
		CKANGroup ckanGroup = new CKANGroup();
		int count = ckanGroup.count();
		logger.debug("The groups are {}", count);
	}
	
	@Ignore
	@Test
	public void list() throws Exception {
		CKANGroup ckanGroup = new CKANGroup();
		String ret = ckanGroup.list(10000, 0);
		logger.debug("{}", ret);
	}
	
	@Ignore
	@Test
	public void read() throws Exception {
		CKANGroup ckanGroup = new CKANGroup();
		ckanGroup.setApiKey(CKANUtility.getSysAdminAPI());
		String name = "abundance-level";
		ckanGroup.setName(name);
		String ret = ckanGroup.read();
		logger.debug("{}", ret);
	}
	
	@Ignore
	@Test
	public void createDeleteGroup() throws Exception {
		CKANGroup ckanGroup = new CKANGroup();
		ckanGroup.setApiKey(CKANUtility.getSysAdminAPI());
		String name = "my-test-group";
		ckanGroup.setName(name);
		ckanGroup.create();
		ckanGroup.delete(true);
	}
	
	public static List<String> listGroup() throws Exception {
		CKANGroup group = new CKANGroup();
		String groups = group.list(1000, 0);
		ObjectMapper objectMapper = new ObjectMapper();
		String[] groupArray = objectMapper.readValue(groups, String[].class);
		return Arrays.asList(groupArray);
	}
	
	// @Test
	public void listAllGroups() throws Exception {
		ContextTest.setContextByName("");
		CKANGroup ckanGroup = new CKANGroup();
		ckanGroup.setApiKey(CKANUtility.getSysAdminAPI());
		List<String> groups = listGroup();
		logger.debug("Groups are:\n{}", groups);
	}
	
	// @Test
	public void deleteAllGroups() throws Exception {
		ContextTest.setContextByName("/gcube");
		CKANGroup ckanGroup = new CKANGroup();
		ckanGroup.setApiKey(CKANUtility.getSysAdminAPI());
		List<String> groups = listGroup();
		for(String name : groups) {
			ckanGroup.setName(name);
			// ckanGroup.delete(true);
		}
	}

}
