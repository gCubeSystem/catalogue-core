package org.gcube.gcat.persistence.ckan;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.ContextTest;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CKANInstanceTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANInstanceTest.class);
	
	@Test
	public void testSerialization() throws Exception {
		CatalogueConfiguration configuration = CatalogueConfigurationFactory.getInstance();
		ObjectMapper mapper = new ObjectMapper();
		logger.debug("Configuration is {}", mapper.writeValueAsString(configuration));
	}
	
}
