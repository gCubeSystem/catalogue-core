package org.gcube.gcat.persistence.ckan;

import java.util.Arrays;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.gcat.ContextTest;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CKANOrganizationTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANOrganizationTest.class);
	
	/*
	@Test
	public void getUserRole() throws Exception {
		CKANOrganization ckanOrganization = new CKANOrganization();
		ckanOrganization.setApiKey(CKANUtility.getSysAdminAPI());
		ckanOrganization.setName(CKANOrganization.getCKANOrganizationName());
		String ret = ckanOrganization.getUserRole("luca.frosini");
		logger.debug("{}", ret);
	}
	*/
	
	@Test
	public void countOrganizations() throws Exception {
		CKANOrganization ckanOrganization = new CKANOrganization();
		int count = ckanOrganization.count();
		logger.debug("The organizations are {}", count);
	}
	
	public static List<String> listOrg() throws Exception {
		CKANOrganization ckanOrganization = new CKANOrganization();
		String ret = ckanOrganization.list(1000, 0);
		ObjectMapper objectMapper = new ObjectMapper();
		String[] groupArray = objectMapper.readValue(ret, String[].class);
		return Arrays.asList(groupArray);
	}
	
	@Test
	public void listOrganizations() throws Exception {
		CKANOrganization ckanOrganization = new CKANOrganization();
		String ret = ckanOrganization.list(1000, 0);
		logger.debug("{}", ret);
	}
	
	@Ignore
	@Test
	public void deleteOrganization() throws Exception {
		ContextTest.setContextByName("/gcube");
		CKANOrganization ckanOrganization = new CKANOrganization();
		ckanOrganization.setApiKey(CKANUtility.getSysAdminAPI());
		String name = "aquamaps1";
		ckanOrganization.setName(name);
		logger.debug("Going to delete {}", name);
//		ckanOrganization.delete(true);
	}
	
	
	@Ignore
	@Test
	public void createOrganization() throws Exception {
		ContextTest.setContextByName("/gcube/devNext/NextNext");
		String context = SecretManagerProvider.instance.get().getContext();
		ScopeBean scopeBean = new ScopeBean(context);
		CKANOrganization ckanOrganization = new CKANOrganization();
		ckanOrganization.setApiKey(CKANUtility.getSysAdminAPI());
		String name = CatalogueConfiguration.getOrganizationName(context);
		ckanOrganization.setName(name);
		String json = "{\"display_name\": \"" + scopeBean.name() + "\",\"description\": \"" + context + " Organization\",\"name\": \"" + name + "\"}";
		logger.info("Going to create Organization {} : {}", name, json);
//		ckanOrganization.create(json);
	}
	
	
	//@Ignore
	@Test
	public void deleteAllOrganizations() throws Exception {
		ContextTest.setContextByName("/gcube");
		CKANOrganization ckanOrganization = new CKANOrganization();
		ckanOrganization.setApiKey(CKANUtility.getSysAdminAPI());
		String ret = ckanOrganization.list(1000, 0);
		logger.debug("{}", ret);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode organizations = (ArrayNode) mapper.readTree(ret);
		for(JsonNode jn : organizations) {
			ckanOrganization.setName(jn.asText());
			logger.debug("Going to delete organization '{}'", jn.asText());
			try {
//				ckanOrganization.delete(true);
			}catch (Exception e) {
				// A not empty organization cannot be deleted
			}
		}
	}
}
