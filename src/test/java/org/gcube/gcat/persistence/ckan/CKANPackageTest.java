package org.gcube.gcat.persistence.ckan;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.gcat.ContextTest;
import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.api.moderation.Moderated;
import org.gcube.gcat.jsonpath.JsonPathTester;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANPackageTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANPackageTest.class);
	
	private static final String NOTES_KEY = "notes";
	private static final String URL_KEY = "url";
	private static final String PRIVATE_KEY = "private";
	
	public static final String ITEM_NAME_VALUE = "restful_transaction_model";
	private static final String LICENSE_VALUE = "CC-BY-SA-4.0";
	private static final String EXTRAS_TYPE_VALUE_VALUE = "EmptyProfile";
	
	@Test
	public void count() throws Exception {
		CKANPackage ckanPackage = new CKANPackage();
		int count = ckanPackage.count();
		logger.debug("Number of items in {} is {}", SecretManagerProvider.instance.get().getContext(), count);
	}
	
	@Test
	public void list() throws Exception {
		ContextTest.setContextByName("/gcube/devsec/devVRE");
		CKANPackage ckanPackage = new CKANPackage();
		
		MultivaluedMap<String, String> mvm = new MultivaluedHashMap<String,String>();
		// mvm.add(Moderated.CM_ITEM_STATUS_QUERY_PARAMETER, CMItemStatus.PENDING.getValue());
		
		UriInfo uriInfo = getUriInfo(mvm);
		ckanPackage.setUriInfo(uriInfo);
		
		ObjectMapper mapper = new ObjectMapper();
		String ret = ckanPackage.list(10, 0);
		JsonNode gotList = mapper.readTree(ret);
		Assert.assertTrue(gotList instanceof ArrayNode);
		logger.debug("List:\n{}", mapper.writeValueAsString(gotList));
	}
	
	public static UriInfo getUriInfo(MultivaluedMap<String, String> queryParameters) {
		UriInfo uriInfo = new UriInfo() {
			
			@Override
			public URI resolve(URI uri) {
				return null;
			}
			
			@Override
			public URI relativize(URI uri) {
				return null;
			}
			
			@Override
			public UriBuilder getRequestUriBuilder() {
				return null;
			}
			
			@Override
			public URI getRequestUri() {
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getQueryParameters(boolean decode) {
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getQueryParameters() {
				return queryParameters;
			}
			
			@Override
			public List<PathSegment> getPathSegments(boolean decode) {
				return null;
			}
			
			@Override
			public List<PathSegment> getPathSegments() {
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getPathParameters(boolean decode) {
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getPathParameters() {
				return null;
			}
			
			@Override
			public String getPath(boolean decode) {
				return null;
			}
			
			@Override
			public String getPath() {
				return null;
			}
			
			@Override
			public List<String> getMatchedURIs(boolean decode) {
				return null;
			}
			
			@Override
			public List<String> getMatchedURIs() {
				return null;
			}
			
			@Override
			public List<Object> getMatchedResources() {
				return null;
			}
			
			@Override
			public UriBuilder getBaseUriBuilder() {
				return null;
			}
			
			@Override
			public URI getBaseUri() {
				return null;
			}
			
			@Override
			public UriBuilder getAbsolutePathBuilder() {
				return null;
			}
			
			@Override
			public URI getAbsolutePath() {
				return null;
			}
		};
		
		return uriInfo;
	}
	
	@Test
	public void listWithParameters() throws Exception {
		String contextName = "/gcube/devNext/NextNext";
		String ckanOrganizationName = CatalogueConfiguration.getOrganizationName(contextName);
		ContextTest.setContextByName(contextName);
		
		CKANPackage ckanPackage = new CKANPackage();
		ObjectMapper mapper = new ObjectMapper();
		
		boolean[] values = new boolean[]{true, false};
		for(boolean includeValidOrganization : values) {
			for(boolean includeFakeOrganization : values) {
			
				MultivaluedMap<String,String> queryParameters = new MultivaluedHashMap<>();
				
				StringWriter stringWriter = new StringWriter();
				
				boolean addOr = false;
				if(includeFakeOrganization) {
					stringWriter.append("organization:fakeorganization");
					addOr = true;
				}
					
				if(includeValidOrganization) {
					if(addOr) {
						stringWriter.append(" OR ");
					}
					stringWriter.append("organization:");
					stringWriter.append(ckanOrganizationName);
				}
				
				String filter = stringWriter.toString();
				if(filter.length()>0) {
					queryParameters.add(GCatConstants.Q_KEY, filter);
				}
				
				
				queryParameters.add(GCatConstants.OWN_ONLY_QUERY_PARAMETER, Boolean.TRUE.toString());
				
				/*
				queryParameters.add("fl","[\"name\"]");
				*/
				/*
				queryParameters.add("facet.field","[\"name\"]");
				queryParameters.add("sort","name asc");
				*/
				
				Map<String,String> parameters = null;
				try  {
					ckanPackage.setUriInfo(getUriInfo(queryParameters));
					parameters = ckanPackage.getListingParameters(10, 0);
				}catch (ForbiddenException e) {
					if(includeFakeOrganization) {
						// This is the expected behaviour
						continue;
					}else {
						throw e;
					}
				}
	
				String ret = ckanPackage.list(parameters);
				JsonNode gotList = mapper.readTree(ret);
				Assert.assertTrue(gotList instanceof ArrayNode);
				
				logger.debug("List :\n{}", mapper.writeValueAsString(gotList));
			}
		}
			
	}
	
	
	@Test(expected=ForbiddenException.class)
	public void checkParameter() throws Exception {
		ContextTest.setContextByName("/gcube/devNext/NextNext");
		
		Map<String, String> parameters = new HashMap<>();
		
		CKANPackage ckanPackage = new CKANPackage();
		
		MultivaluedMap<String,String> queryParameters = new MultivaluedHashMap<>();
		queryParameters.add(GCatConstants.Q_KEY, "organization:nextnext OR organization:fakeorg");
		
		parameters = ckanPackage.checkListParameters(queryParameters, parameters);
		
		logger.debug("{}", parameters);
	}
	
	
	/*
	 * PRE
	 * 
	 * Workspace(luca.frosini) > RESTful Transaction Model.pdf
	 * https://data1-d.d4science.org/shub/E_YjI4STdKKzRlNjgzMm9jQWxjcmtReDNwbDFYR3lpTHo3SjdtN1RDZ3c2OGk0ZHZhdE5iZElBKzNxUDAyTGFqZw==
	 * https://goo.gl/HcUWni
	 * 
	 * 
	 * Workspace(luca.frosini) > RESTful Transaction Model v 1.0.pdf
	 * https://data1-d.d4science.org/shub/E_aThRa1NpWFJpTGEydEU2bEJhMXNjZy8wK3BxekJKYnpYTy81cUkwZVdicEZ0aGFRZmY4MkRnUC8xWW0zYzVoVg==
	 * https://goo.gl/J8AwQW
	 * ContextTest.setContextByName("/gcube/devsec/devVRE");
		
	 * 
	 * Workspace(luca.frosini) > RESTful Transaction Model v 1.1.pdf
	 * https://data1-d.d4science.org/shub/E_NkhrbVV4VTluT0RKVUtCRldobFZTQU5ySTZneFdpUzJ2UjJBNlZWNDlURDVHamo4WjY5RnlrcHZGTGNkT2prUg==
	 * https://goo.gl/78ViuR
	 * 
	 */
	
	@Test
	public void testNameRegex() {
		Map<String,Boolean> stringsToTest = new HashMap<>();
		stringsToTest.put("Test", false); // Fails for T
		stringsToTest.put("test-test+test-test", false); // Fails for +
		stringsToTest.put("t", false); // Fails because is too short. Min length is 2 characters
		stringsToTest.put("te", true);
		stringsToTest.put("test-test_test-test", true);
		stringsToTest.put(
				"test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_",
				true);
		// // Fails because is too long. Max length is 100 characters
		stringsToTest.put(
				"test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_test-test_t",
				false);
		
		for(String testString : stringsToTest.keySet()) {
			boolean match = testString.matches(CKANPackage.NAME_REGEX);
			logger.debug("'{}' does {}match the regex {}", testString, match ? "" : "NOT ", CKANPackage.NAME_REGEX);
			Assert.assertEquals(stringsToTest.get(testString), match);
		}
		
	}
	
	protected CKANPackage createPackage(ObjectMapper mapper, Boolean socialPost) throws Exception {
		String currentContext = SecretManagerProvider.instance.get().getContext();
		String organization = CatalogueConfiguration.getOrganizationName(currentContext);
		
		ObjectNode itemObjectNode = mapper.createObjectNode();
		itemObjectNode.put(CKAN.NAME_KEY, ITEM_NAME_VALUE);
		itemObjectNode.put(CKANPackage.TITLE_KEY, "RESTful Transaction Model");
		itemObjectNode.put(CKANPackage.LICENSE_KEY, LICENSE_VALUE);
		itemObjectNode.put(PRIVATE_KEY, false);
		itemObjectNode.put(NOTES_KEY, "A research of Luca Frosini");
		itemObjectNode.put(URL_KEY, "https://www.d4science.org");
		itemObjectNode.put(CKANPackage.OWNER_ORG_KEY, organization);
		
		ArrayNode tagArrayNode = itemObjectNode.putArray(CKANPackage.TAGS_KEY);
		ObjectNode tagNode = mapper.createObjectNode();
		tagNode.put(CKANPackage.NAME_KEY, "REST");
		tagArrayNode.add(tagNode);
		
		ArrayNode resourceArrayNode = itemObjectNode.putArray(CKANPackage.RESOURCES_KEY);
		ObjectNode resourceNode = mapper.createObjectNode();
		resourceNode.put(CKANResource.NAME_KEY, "RESTful Transaction Model");
		// Workspace(luca.frosini) > RESTful Transaction Model v 1.1.pdf
		resourceNode.put(CKANResource.URL_KEY, "https://data-dev.d4science.net/Qpw2");
		resourceArrayNode.add(resourceNode);
		
		ArrayNode extraArrayNode = itemObjectNode.putArray(CKANPackage.EXTRAS_KEY);
		ObjectNode typeNode = mapper.createObjectNode();
		typeNode.put(CKANPackage.EXTRAS_KEY_KEY, CKANPackage.EXTRAS_KEY_VALUE_SYSTEM_TYPE);
		typeNode.put(CKANPackage.EXTRAS_VALUE_KEY, EXTRAS_TYPE_VALUE_VALUE);
		extraArrayNode.add(typeNode);
		
		/*
		ObjectNode modelNode = mapper.createObjectNode();
		modelNode.put(CKANPackage.EXTRAS_KEY_KEY, "test");
		modelNode.put(CKANPackage.EXTRAS_VALUE_KEY, "test 2.9°");
		extraArrayNode.add(modelNode);
		*/
		
		ObjectNode populationNode = mapper.createObjectNode();
		populationNode.put(CKANPackage.EXTRAS_KEY_KEY, "Population");
		populationNode.put(CKANPackage.EXTRAS_VALUE_KEY, "Italian");
		extraArrayNode.add(populationNode);
		
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		//ckanPackage.setApiKey(CKANUtility.getSysAdminAPI());
	
		MultivaluedMap<String, String> mvm = new MultivaluedHashMap<String,String>();
		mvm.add(GCatConstants.SOCIAL_POST_QUERY_PARAMETER, socialPost.toString());
		UriInfo uriInfo = getUriInfo(mvm);
		ckanPackage.setUriInfo(uriInfo);
		
		String createdItem = ckanPackage.create(mapper.writeValueAsString(itemObjectNode));
		logger.debug(createdItem);
		
		return ckanPackage;
	}
	
	@Test
	public void create() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		createPackage(mapper, true);
	}
	
	@Test
	public void testURIResolver() {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName("Test");
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		ckanPackage.addItemURLViaResolver(objectNode);
	}
	
	@Test
	public void createReadUpdateUpdatePurge() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		createPackage(mapper, false);
		
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		String readItem = ckanPackage.read();
		
		JsonNode readItemObjectNode = mapper.readTree(readItem);
		String readID = readItemObjectNode.get(CKANPackage.NAME_KEY).asText();
		Assert.assertNotNull(readID);
		
		Assert.assertTrue(ITEM_NAME_VALUE.compareTo(readID) == 0);
		
		String updatedNotes = "A research of Luca Frosini made during the PhD";
		((ObjectNode) readItemObjectNode).put(NOTES_KEY, updatedNotes);
		
//		ArrayNode resources = (ArrayNode) readItemObjectNode.get(CKANPackage.RESOURCES_KEY);
//		ObjectNode objectNode = (ObjectNode) resources.get(0);
//		// Workspace(luca.frosini) > RESTful Transaction Model v 1.1.pdf
//		objectNode.put(CKANResource.URL_KEY, "https://data-dev.d4science.net/Qpw2");
//		resources.set(0, objectNode);
//		
//		((ObjectNode) readItemObjectNode).replace(CKANPackage.RESOURCES_KEY, resources);
		
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		String updatedItem = ckanPackage.update(mapper.writeValueAsString(readItemObjectNode));
		logger.trace(updatedItem);
		JsonNode updatedItemObjectNode = mapper.readTree(updatedItem);
		String gotUpdatedNotes = updatedItemObjectNode.get(NOTES_KEY).asText();
		Assert.assertTrue(gotUpdatedNotes.compareTo(updatedNotes) == 0);
		
		/*
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		((ObjectNode) updatedItemObjectNode).remove(CKANPackage.RESOURCES_KEY);
		String secondUpdateItem = ckanPackage.update(mapper.writeValueAsString(updatedItemObjectNode));
		logger.trace(secondUpdateItem);
		*/
		
		/*
		ObjectNode patchObjectNode = mapper.createObjectNode();
		String patchedNotes = updatedNotes + " in October 2018";
		patchObjectNode.put(NOTES_KEY, patchedNotes);
		patchObjectNode.put(CKANPackage.NAME_KEY, ITEM_NAME_VALUE);
		
		ckanPackage =  new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		String patchedItem = ckanPackage.patch(mapper.writeValueAsString(patchObjectNode));
		logger.trace(patchedItem);
		JsonNode patchedItemObjectNode = mapper.readTree(patchedItem);
		String gotPatchedNotes = patchedItemObjectNode.get(NOTES_KEY).asText();
		Assert.assertTrue(gotPatchedNotes.compareTo(patchedNotes)==0);
		*/
		
		/*
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.purge();
		logger.debug("Item {} purge successfully", ITEM_NAME_VALUE);
		*/
	}
	
	@Test
	//(expected = NotFoundException.class)
	public void read() throws Exception {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		String ret = ckanPackage.read();
		logger.debug(ret);
	}
	
//	@Ignore
	@Test
	//(expected = NotFoundException.class)
	public void delete() throws Exception {
		delete(true);
	}
	
	public void delete(boolean purge) throws Exception {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.delete(purge);
	}
	
	@Test
	public void testUpdate() {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName("knime_workflow_with_joined_consumer_phase_and_dose_response_model");
		ckanPackage.update("{\"rating\":0.0,\"license_title\":\"Academic Free License 3.0\",\"maintainer\":\"\",\"relationships_as_object\":[],\"private\":false,\"maintainer_email\":\"\",\"num_tags\":5,\"id\":\"f4292d0e-c94f-4542-bfa3-25f78638fc1b\",\"metadata_created\":\"2020-01-07T16:40:16.987780\",\"owner_org\":\"3571cca5-b0ae-4dc6-b791-434a8e062ce5\",\"metadata_modified\":\"2020-02-03T14:24:59.221160\",\"author\":\"Buschhardt Tasja\",\"author_email\":\"tasja.buschhardt@bfr.bund.de\",\"state\":\"active\",\"version\":\"1\",\"license_id\":\"AFL-3.0\",\"type\":\"dataset\",\"resources\":[{\"cache_last_updated\":null,\"cache_url\":null,\"mimetype_inner\":null,\"hash\":\"\",\"description\":\"\",\"format\":\"knwf\",\"url\":\"https://data.d4science.net/g9QY\",\"created\":\"2019-09-16T20:49:02.168666\",\"state\":\"active\",\"package_id\":\"f4292d0e-c94f-4542-bfa3-25f78638fc1b\",\"last_modified\":null,\"mimetype\":null,\"url_type\":null,\"position\":0,\"revision_id\":\"a84a35ec-2786-4835-9f50-ad52202c4e33\",\"size\":null,\"datastore_active\":false,\"id\":\"0734b380-ea5d-4c99-be03-c38ff6ae6fbf\",\"resource_type\":null,\"name\":\"KNIME_WF_ICPMF11\"}],\"num_resources\":1,\"tags\":[{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"Campylobacter\",\"id\":\"84c76669-d135-4c5e-9e3a-b163689a10de\",\"name\":\"Campylobacter\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"KNIME workflow\",\"id\":\"30bce4d2-fc45-46ab-8f8b-5da582fff3c3\",\"name\":\"KNIME workflow\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"chicken meat\",\"id\":\"f1aac698-a865-4bf4-ac55-b53f8bf7ecac\",\"name\":\"chicken meat\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"consumer phase model\",\"id\":\"9e6b2337-9ac0-4bfc-8e7f-4327c531bbec\",\"name\":\"consumer phase model\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"dose response model\",\"id\":\"21311c09-a928-4a9a-83de-7ef98b257af5\",\"name\":\"dose response model\"}],\"groups\":[],\"creator_user_id\":\"7020f836-45f4-4ee8-9c65-e7504209644f\",\"relationships_as_subject\":[],\"name\":\"knime_workflow_with_joined_consumer_phase_and_dose_response_model\",\"isopen\":true,\"url\":\"\",\"notes\":\"This KNIME workflow shows how to use FSK-Lab to read, customise, run, \\r\\ncombine and change simulation settings for food safety models- specifically \\r\\nthis workflow exemplifies a joined consumer phase model for Campylobacter \\r\\nin chicken meat and a dose response model for Campylobacter\",\"title\":\"KNIME workflow with joined consumer phase and dose response model\",\"extras\":[{\"value\":\"\",\"key\":\"Author\"},{\"value\":\"https://data.d4science.org/ctlg/RAKIP_trial/knime_workflow_with_joined_consumer_phase_and_dose_response_model\",\"key\":\"Item URL\"},{\"value\":\"ResearchObject\",\"key\":\"system:type\"},{\"value\":\"{\\\"relatedIdentifierType\\\":\\\"URL\\\",\\\"relationType\\\":\\\"isReferencedBy\\\",\\\"link\\\":\\\"https://doi.org/10.5072/zenodo.488235\\\",\\\"zenodoId\\\":488235}\",\"key\":\"relatedIdentifier:Zenodo.DOI\"}],\"license_url\":\"https://www.opensource.org/licenses/AFL-3.0\",\"ratings_count\":0,\"organization\":{\"description\":\"\",\"title\":\"devVRE\",\"created\":\"2016-05-30T11:30:41.710079\",\"approval_status\":\"approved\",\"is_organization\":true,\"state\":\"active\",\"image_url\":\"\",\"revision_id\":\"7c8463df-ed3f-4d33-87d8-6c0bcbe30d5d\",\"type\":\"organization\",\"id\":\"3571cca5-b0ae-4dc6-b791-434a8e062ce5\",\"name\":\"devvre\"},\"revision_id\":\"9f51fa28-0732-46c9-a208-9a0e6da0cd2c\"}");
	}
	
	@Test
	public void testUpdate2() {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName("vre_picture");
		ckanPackage.update("{\"rating\": 0.0, \"license_title\": \"Academic Free License 3.0\", \"maintainer\": \"Kerekes Kata\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"kerekeska@nebih.gov.hu\", \"num_tags\": 1, \"id\": \"7731b70f-47ff-4b74-b943-188215e82d07\", \"metadata_created\": \"2020-01-07T16:40:24.822719\", \"owner_org\": \"3571cca5-b0ae-4dc6-b791-434a8e062ce5\", \"metadata_modified\": \"2020-02-03T15:16:42.596068\", \"author\": \"Kerekes Kata\", \"author_email\": \"kerekeska@nebih.gov.hu\", \"state\": \"active\", \"version\": \"1\", \"license_id\": \"AFL-3.0\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"cache_url\": null, \"mimetype_inner\": null, \"hash\": \"\", \"description\": \"\", \"format\": \"JPEG\", \"url\": \"https://goo.gl/SnwAM7\", \"created\": \"2019-04-01T13:24:40.738838\", \"state\": \"active\", \"package_id\": \"7731b70f-47ff-4b74-b943-188215e82d07\", \"last_modified\": null, \"mimetype\": \"image/jpeg\", \"url_type\": null, \"position\": 0, \"revision_id\": \"06d61000-a0c1-4155-ad2d-78ede56d6bb5\", \"size\": null, \"datastore_active\": false, \"id\": \"1de8851d-1385-47ae-9c93-6040d170a9cc\", \"resource_type\": null, \"name\": \"th.jpeg\"}], \"num_resources\": 1, \"tags\": [{\"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"DEMETER\", \"id\": \"4e05058b-a006-4dbf-94f5-277a30318323\", \"name\": \"DEMETER\"}], \"groups\": [], \"creator_user_id\": \"7020f836-45f4-4ee8-9c65-e7504209644f\", \"relationships_as_subject\": [], \"name\": \"vre_picture\", \"isopen\": true, \"url\": \"\", \"notes\": \"This is a nice picture of a VRE ;)\", \"title\": \"VRE picture\", \"extras\": [{\"value\": \"https://data.d4science.org/ctlg/DEMETER_trial/vre_picture\", \"key\": \"Item URL\"}, {\"value\": \"ResearchObject\", \"key\": \"system:type\"}], \"license_url\": \"https://www.opensource.org/licenses/AFL-3.0\", \"ratings_count\": 0, \"organization\": {\"description\": \"\", \"title\": \"devVRE\", \"created\": \"2016-05-30T11:30:41.710079\", \"approval_status\": \"approved\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"\", \"revision_id\": \"7c8463df-ed3f-4d33-87d8-6c0bcbe30d5d\", \"type\": \"organization\", \"id\": \"3571cca5-b0ae-4dc6-b791-434a8e062ce5\", \"name\": \"devvre\"}, \"revision_id\": \"bdb6169a-6268-43d6-b7e1-265c0c9e1a1c\"}");
	}

	public File getExamplesDirectory() throws Exception {
		URL logbackFileURL = JsonPathTester.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "examples");
	}
	
	
	protected String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(file.toPath());
		return new String(encoded, Charset.defaultCharset());
	}
	
	@Test(expected = BadRequestException.class)
	public void testCreateWithModerationSystemMetadata() throws Exception {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName("a_test_item");
		File examplesDir = getExamplesDirectory();
		File createFile = new File(examplesDir, "a_test_item_create_ko.json");
		String json = readFile(createFile);
		try {
			ckanPackage.create(json);
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	@Test(expected = BadRequestException.class)
	public void testUpdateWithChangedModerationSystemMetadata() throws Exception {
		try {
			CKANPackage ckanPackage = new CKANPackage();
			ckanPackage.setName("a_test_item");
			File examplesDir = getExamplesDirectory();
			File createFile = new File(examplesDir, "a_test_item_create_ok.json");
			String json = readFile(createFile);
			json = ckanPackage.create(json);
			JsonNode jsonNode = ckanPackage.mapper.readTree(json);
			ckanPackage.setItemToPending(jsonNode);
			String updateJson = ckanPackage.mapper.writeValueAsString(jsonNode);
			ckanPackage.update(updateJson);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}finally {
			CKANPackage ckanPackage = new CKANPackage();
			ckanPackage.setName("a_test_item");
			ckanPackage.purge();
		}
		
	}
	
	@Test
	public void generateLinoTokenModeration() throws Exception {
		UserInfo userInfo = new UserInfo("leonardo.candela", new ArrayList<>());
		String userToken = authorizationService().generateUserToken(userInfo, VRE);
		logger.debug(userToken);
	}
	
	
	@Test
	public void testModeration() throws Exception {
		CKANPackage ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		try {
			ckanPackage.purge();
		}catch (NotFoundException e) {
			logger.trace("The item does not exist. This is correct");
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		createPackage(mapper, false);
		
		
		ContextTest.setContextByName("leonardo.candela_"+VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.message("Please add the notes.");
		
		
		ContextTest.setContextByName(VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		String readItem = ckanPackage.read();
		JsonNode readItemObjectNode = mapper.readTree(readItem);
		String updatedNotes = "A research of Luca Frosini made during the PhD";
		((ObjectNode) readItemObjectNode).put(NOTES_KEY, updatedNotes);
		String ret = ckanPackage.update(mapper.writeValueAsString(readItemObjectNode));
		logger.debug("Updated {}", ret);
		
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.message("I hope now it can be approved.");
		
		
		ContextTest.setContextByName("leonardo.candela_"+VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.reject("You must specify the institution.");
		
		
		ContextTest.setContextByName(VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		readItem = ckanPackage.read();
		readItemObjectNode = mapper.readTree(readItem);
		updatedNotes = "A research of Luca Frosini at ISTI";
		((ObjectNode) readItemObjectNode).put(NOTES_KEY, updatedNotes);
		ckanPackage.update(mapper.writeValueAsString(readItemObjectNode));
		
		
		ContextTest.setContextByName("pasquale.pagano_"+VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.approve("It seems fine now");
		
		
		ContextTest.setContextByName(VRE);
		ckanPackage = new CKANPackage();
		ckanPackage.setName(ITEM_NAME_VALUE);
		ckanPackage.purge();
	}
	
	@Ignore
//	@Test
	public void deleteAllInASpecificOrganization() {
		CKANPackage ckanPackage = new CKANPackage();
		MultivaluedMap<String, String> mvm = new MultivaluedHashMap<String,String>();
		mvm.add(GCatConstants.Q_KEY, "organization:devvre");
		mvm.add(GCatConstants.OWN_ONLY_QUERY_PARAMETER, "false");
		UriInfo uriInfo = getUriInfo(mvm);
		ckanPackage.setUriInfo(uriInfo);
//		String res = ckanPackage.deleteAll(true);
//		logger.debug("{}", res);
	}
	
//	@Ignore
	@Test
	public void deleteAllItemsInAllOrganizations() {
		CKANPackage ckanPackage = new CKANPackage();
		MultivaluedMap<String, String> mvm = new MultivaluedHashMap<String,String>();
		mvm.add(GCatConstants.OWN_ONLY_QUERY_PARAMETER, "false");
		UriInfo uriInfo = getUriInfo(mvm);
		ckanPackage.setUriInfo(uriInfo);
		String res = ckanPackage.deleteAll(true);
		logger.debug("{}", res);
	}
	
	@Test
	public void listTest() {
		CKANPackage ckanPackage = new CKANPackage();
		MultivaluedMap<String, String> mvm = new MultivaluedHashMap<String,String>();
		mvm.add(GCatConstants.OWN_ONLY_QUERY_PARAMETER, "false");
		UriInfo uriInfo = getUriInfo(mvm);
		ckanPackage.setUriInfo(uriInfo);
		String res = ckanPackage.list(10, 0);
		logger.debug("{}", res);
	}
	
	@Test
	public void teet() {
		Boolean b = Boolean.parseBoolean("false");
		logger.debug("{}", b);
		b = Boolean.parseBoolean("False");
		logger.debug("{}", b);
	}
	
	
}
