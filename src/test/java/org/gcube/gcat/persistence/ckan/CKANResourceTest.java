package org.gcube.gcat.persistence.ckan;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.gcat.ContextTest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CKANResourceTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(CKANResourceTest.class);
	
	@Ignore
	@Test
	public void testCopyStorageResource() throws Exception {
		/* 
		 * ContextTest.setContextByName(GCUBE);
		 * URL url = new URL("https://data.dev.d4science.org/shub/E_QllPeWdoT09Qb0piUkwzdDBQUjhvbWFLT0kyQ3hrOHkzSFo0TXg4bkFvSnlnU1gvQVJxcW40eVBkWVFmQWVvTQ==");
		 */
		
		ContextTest.setContextByName(ROOT_PRE);
		URL url = new URL("https://data-pre.d4science.org/shub/E_amYzbG9MaitXMW1JelJ3a1BqN3gwV29Kazh5V2wyUTJzQTZ2d1NIRDZlNmxjdVhUa0E2d1BlNmJDT0UzanZLWA==");
		
		String itemID = UUID.randomUUID().toString();
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.resourceID = UUID.randomUUID().toString();
		URL finalURL = ckanResource.copyStorageResource(url);
		logger.debug("Initial URL is  {} - Persisted File URL is {}", url, finalURL);
		//ckanResource.deleteStorageResource(finalURL, ckanResource.resourceID, ckanResource.mimeType);
		ckanResource.deleteStorageResource(finalURL);
	}
	
	@Ignore
	@Test
	public void testCreate() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.put(CKANResource.NAME_KEY, "MyTestTy_rest_upload");
		objectNode.put(CKANResource.URL_KEY, "https://data.d4science.org/shub/58a13287-3e91-4afd-bd80-cf4605a0edaa");
		objectNode.put("description", " uploaded this file using the REST API");
		// objectNode.put(CKANResource.ID_KEY, "ba7ab7e8-c268-4219-98cd-c73470870999");
		
		CKANResource ckanResource = new CKANResource("ba7ab7e8-c268-4219-98cd-c73470870999");
		String json = ckanResource.getAsString(objectNode);
		logger.debug("Going to create Resource {}", json);
		ckanResource.create(objectNode);
	}
	
	@Test
	public void testCreateUpdate() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.put(CKANResource.NAME_KEY, "MyTestTy_rest_upload");
		objectNode.put(CKANResource.URL_KEY, "https://data-dev.d4science.net/C1G2");
		objectNode.put("description", "File uploaded using the REST API");
		// objectNode.put(CKANResource.ID_KEY, "ba7ab7e8-c268-4219-98cd-c73470870999");
		
		String id = "5795a7ce-4444-439e-b77d-1df3beaa8a42"; // name = my_restful
		CKANResource ckanResource = new CKANResource(id);
		String json = ckanResource.getAsString(objectNode);
		logger.debug("Going to create Resource {}", json);
		String createdJson = ckanResource.create(objectNode);
		
		ckanResource = new CKANResource(id);
		ckanResource.update(createdJson);
	}
	
	// @Test
	public void testDelete() throws Exception {
		CKANResource ckanResource = new CKANResource("f0326fec-d8ac-42c7-abff-c7905b4d938e");
		ckanResource.setResourceID("fcf98272-41e7-4f05-9294-fdafb1a33074");
		ckanResource.delete();
	}
	
	@Test
	public void testGetFormat() {
		CKANResource ckanResource = new CKANResource(UUID.randomUUID().toString());
		ckanResource.mimeType = "application/zip";
		
		ckanResource.originalFileExtension = ".fskk";
		String format = ckanResource.getFormat();
		Assert.assertTrue(format.compareTo("fskk") == 0);
		
		ckanResource.originalFileExtension = null;
		format = ckanResource.getFormat();
		Assert.assertTrue(format.compareTo("zip") == 0);
		
		ckanResource.mimeType = "image/jpeg";
		format = ckanResource.getFormat();
		Assert.assertTrue(format.compareTo("jpg") == 0);
	}
	
	@Test
	public void testFinalURIResolverURL() throws Exception {
		List<String> urlsString = new ArrayList<>();
		
		urlsString.add("https://data-pre.d4science.net/wgsZ");
		
		urlsString.add("https://data-dev.d4science.net/TzQv");
		urlsString.add("https://data-d.d4science.org/shub/E_MkxMbitjYzY3R3VlTmp1cDF6cHVHY2w1Zk15RTdFakUrZnpKYXVLN1R6T0dtT1FXaXpIOHJmb2dSZ2p4WS9hYw==");
		
		urlsString.add("https://data.d4science.net/Cuvn");
		
		//urlsString.add("");
		
		for(String urlString : urlsString) {
			try {
				URL url = CKANResource.getFinalURL(new URL(urlString));
				System.out.println(urlString + " -> " + url.toString());
				logger.debug("{} -> {}", urlString, url.toString());
			}catch (Exception e) {
				System.out.println("Error while getting final URL for " + urlString);
				logger.debug("Error while getting final URL for {}", urlString);
			}
		}
	}
	
}
