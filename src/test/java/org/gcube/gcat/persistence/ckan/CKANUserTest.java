package org.gcube.gcat.persistence.ckan;

import org.gcube.gcat.ContextTest;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;

public class CKANUserTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANUserTest.class);
	
	private static final String USERNAME = "lucio_lelii";
	
	private CKANUser getCKANUser() {
		CKANUser user = new CKANUser();
		user.setApiKey(CKANUtility.getSysAdminAPI());
		user.setName(USERNAME);
		return user;
	}
	
	@Test
	public void getUsernameFromCkanUsername() {
		String username = "luca.frosini";
		String ckanUsername = CKANUser.getCKANUsername(username);
		Assert.assertTrue(ckanUsername.compareTo("luca_frosini")==0);
		String gotUsername = CKANUser.getUsernameFromCKANUsername(ckanUsername);
		Assert.assertTrue(gotUsername.compareTo(username)==0);
	}
	
	
	@Test
	public void list() throws Exception {
		CKANUser ckanUser = getCKANUser();
		String ret = ckanUser.list(-1, -1);
		logger.debug("{}", ret);
	}
	
	@Test
	public void create() throws Exception {
		CKANUser ckanUser = getCKANUser();
		String ret = ckanUser.createInCkan();
		logger.debug("{}", ret);
	}
	
	@Test
	public void read() throws Exception {
		CKANUser ckanUser = getCKANUser();
		String ret = ckanUser.read();
		logger.debug("{}", ret);
	}
	
	public final static String DISPLAY_NAME = "display_name";
	
	@Test
	public void update() throws Exception {
		CKANUser ckanUser = getCKANUser();
		String ret = ckanUser.read();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode readUser = mapper.readTree(ret);
		((ObjectNode) readUser).put(CKANUser.EMAIL, USERNAME + "@gcube.ckan.org");
		((ObjectNode) readUser).put("state", "active");
		ret = ckanUser.update(ckanUser.getAsString(readUser));
		logger.debug("{}", ret);
	}
	
	@Test
	public void delete() throws Exception {
		CKANUser ckanUser = getCKANUser();
		ckanUser.delete(false);
	}
	
	@Test
	public void testAddSpecialUserToOrganization() throws Exception {
		CKANUser ckanUser = getCKANUser();
		ckanUser.addUserToOrganization();
	}
	
	@Test
	public void checkAndUpdateInformation() throws Exception {
		CKANUser ckanUser = CKANUserCache.getCurrrentCKANUser();
		logger.debug("{}", ckanUser.result);
		
		ckanUser = CKANUserCache.getCurrrentCKANUser();
		logger.debug("{}", ckanUser.result);
		
		ckanUser = CKANUserCache.getCurrrentCKANUser();
		logger.debug("{}", ckanUser.result);
	}
}
