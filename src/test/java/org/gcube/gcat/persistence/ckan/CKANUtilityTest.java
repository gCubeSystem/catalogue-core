package org.gcube.gcat.persistence.ckan;

import org.gcube.gcat.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CKANUtilityTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(CKANPackageTest.class);
	
	@Test
	public void testGetApiKey() throws Exception {
		String ckanAPI = CKANUtility.getApiKey();
		logger.debug("User {} has the following API key {}", CKANUser.getCKANUsername(), ckanAPI);
	}
	
}
