package org.gcube.gcat.persistence.ckan;

import javax.ws.rs.NotFoundException;

import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.gcat.ContextTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class CKanPackageTrashTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(CKanPackageTrashTest.class);
	
	protected boolean find(ArrayNode ids, String name) {
		boolean found = false;
		for(int i=0; i<ids.size(); i++) {
			if(name.compareTo(ids.get(i).asText())==0) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	@Test
	public void testListAndEmptyTrash() throws Exception {
		// Cleanign the env
		ContextTest.setContextByName(VRE);
		CKANPackageTest ckanPackageTest = new CKANPackageTest();
		ckanPackageTest.delete(true);
				
		ckanPackageTest = new CKANPackageTest();
		ckanPackageTest.create();
		ckanPackageTest.delete(false);
		
		CKANPackageTrash ckanPackageTrash = new CKANPackageTrash();
		ArrayNode ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		boolean found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(found);
		
		ckanPackageTest.delete(true);
		ids = ckanPackageTrash.getItems();
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		logger.debug("{}", ids);
	}
	
	public static final String NON_CATALOGUE_ADMIN_USER = "lucio.lelii";
	
	@Test
	public void testListAndEmptyTrashFromAdmin() throws Exception {
		// Cleanign the env
		ContextTest.setContextByName(VRE);
		CKANPackageTest ckanPackageTest = new CKANPackageTest();
		try {
			ckanPackageTest.delete(true);
		}catch (NotFoundException e) {
			// It is expected. the env was clean
		}
		
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER+"_"+VRE);
		ckanPackageTest = new CKANPackageTest();
		ckanPackageTest.create();
		ckanPackageTest.delete(false);
		
		// I'm admin
		ContextTest.setContextByName(VRE);
		CKANPackageTrash ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ArrayNode ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		boolean found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(found);
		
		ckanPackageTrash.setOwnOnly(true);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER+"_"+VRE);
		ckanPackageTest.delete(true);
		
		// I'm admin
		ContextTest.setContextByName(VRE);
		ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ids = ckanPackageTrash.getItems();
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		logger.debug("{}", ids);
	}
	
	
	@Test
	public void testListAndEmptyTrashFromNonAdmin() throws Exception {
		// Cleaning the env
		ContextTest.setContextByName(VRE);
		CKANPackageTest ckanPackageTest = new CKANPackageTest();
		try {
			ckanPackageTest.delete(true);
		}catch (NotFoundException e) {
			// It is expected. the env was clean
		}
		
		ContextTest.setContextByName(VRE);
		ckanPackageTest = new CKANPackageTest();
		ckanPackageTest.create();
		ckanPackageTest.delete(false);
				
		// He is not admin
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER+"_"+VRE);
		CKANPackageTrash ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ArrayNode ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		boolean found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		
		ckanPackageTrash.setOwnOnly(true);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		
		
		ContextTest.setContextByName(VRE);
		ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(found);
		
		ckanPackageTrash.setOwnOnly(true);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(found);
		
		ckanPackageTest.delete(true);
		
		ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		
		ckanPackageTrash.setOwnOnly(true);
		ids = ckanPackageTrash.getItems();
		logger.debug("{}", ids);
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		
		
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER+"_"+VRE);
		ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(false);
		ids = ckanPackageTrash.getItems();
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		logger.debug("{}", ids);
		
		ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(true);
		ids = ckanPackageTrash.getItems();
		found = find(ids, CKANPackageTest.ITEM_NAME_VALUE);
		Assert.assertTrue(!found);
		logger.debug("{}", ids);
	}
}
