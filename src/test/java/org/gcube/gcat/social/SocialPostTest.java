package org.gcube.gcat.social;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.ContextTest;
import org.gcube.gcat.utils.Constants;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocialPostTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(SocialPostTest.class);
	
	@Test
	public void testToken() throws Exception {
		Secret secret = Constants.getCatalogueSecret();
		logger.debug("Application Token is {}", secret.getToken());
		ContextTest.set(secret);
	}
	
	@Test
	public void testSendPost() throws Exception {
		SocialPost socialService = new SocialPost();
		socialService.setItemID(UUID.randomUUID().toString());
		socialService.setItemTitle("Test Item");
		socialService.setItemURL("https://www.d4science.org");
		List<String> tags = new ArrayList<>();
		tags.add("Test");
		tags.add("ThisIsATest");
		tags.add("IgnoreIt");
		socialService.setTags(tags);
		
		socialService.sendSocialPost(false);
	}
	
}
