package org.gcube.gcat.social;

import org.gcube.common.authorization.utils.socialservice.SocialService;
import org.gcube.gcat.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocialServiceTest {
	
	private static Logger logger = LoggerFactory.getLogger(SocialServiceTest.class);
	
	@Test
	public void get() throws Exception {
		ContextTest.setContextByName("/d4science.research-infrastructures.eu/D4Research/AGINFRAplusDev");
		SocialService socialService = SocialService.getSocialService();
		logger.debug(socialService.getServiceBasePath());
	}
}
