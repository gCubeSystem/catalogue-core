package org.gcube.gcat.utils;

import java.net.URL;
import java.util.regex.Pattern;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.gcat.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ConstantsTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ConstantsTest.class);
	
	@Test
	public void testGetApplicationToken() throws Exception {
		logger.debug("Application token for Context {} is {}", SecretManagerProvider.instance.get().getContext(),
				Constants.getCatalogueSecret().getToken());
	}
	
	@Test
	public void decrypt() throws Exception {
		StringEncrypter stringEncrypter = StringEncrypter.getEncrypter();
		String decrypted = stringEncrypter.decrypt("w0KVc+78b2yUQsndDh/cXyyRquuwyILTygmoF0Y5Dls=");
		logger.debug("{}", decrypted);
	}

	@Test
	public void getToken() throws Exception {
		Secret secret = Constants.getCatalogueSecret();
		logger.debug(secret.getToken());
	}	
	
	@Test
	public void getResourceTest() {
		URL url1 = ConstantsTest.class.getResource(Constants.CLIENT_ID_SECRET_FILENAME);
		logger.debug("{}", url1);
		URL url2 = ConstantsTest.class.getClassLoader().getResource(Constants.CLIENT_ID_SECRET_FILENAME);
		logger.debug("{}", url2);
		URL url3 = ClassLoader.getSystemClassLoader().getResource(Constants.CLIENT_ID_SECRET_FILENAME);
		logger.debug("{}", url3);
		
	}
	
	@Test
	public void testRegex() throws Exception {
		String regex = "^([a-zA-ZÀ-ÿ .‘-]+),\\s*([a-zA-ZÀ-ÿ .’-]+)(?:,\\s*([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}))?(?:,\\s*(orcid.org/\\d{4}-\\d{4}-\\d{4}-\\d{3}[0-9X]))?$";
		Pattern.compile(regex);
	}
	
}
