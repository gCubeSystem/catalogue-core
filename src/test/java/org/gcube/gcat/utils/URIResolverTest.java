package org.gcube.gcat.utils;

import java.util.Calendar;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.gcat.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class URIResolverTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(URIResolverTest.class);
	
	@Test
	public void getInstanceTest() throws Exception {
		URIResolver uriResolver = URIResolver.getInstance();
		String catalogueItemURL = uriResolver.getCatalogueItemURL("my_first_restful_transaction_model");
		logger.debug("Item URL is {}", catalogueItemURL);
		
		
		URIResolver.uriResolver = null;
		uriResolver = URIResolver.getInstance();
		catalogueItemURL = uriResolver.getCatalogueItemURL("my_first_restful_transaction_model");
		logger.debug("Item URL is {}", catalogueItemURL);
		
		
		uriResolver.expireTime = Calendar.getInstance();
		uriResolver.expireTime.add(Calendar.MINUTE, -1);
		uriResolver = URIResolver.getInstance();
		catalogueItemURL = uriResolver.getCatalogueItemURL("my_first_restful_transaction_model");
		logger.debug("Item URL is {}", catalogueItemURL);
		
	}
	
	@Test
	public void decrypt() throws Exception {
		ContextTest.setContextByName("/gcube/devsec/devVRE");
		String encrypted = "";
		String pwd = StringEncrypter.getEncrypter().decrypt(encrypted);
		logger.info(pwd);
	}
	
}
